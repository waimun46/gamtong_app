/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */
import 'react-native-gesture-handler';
import React, { Component, Fragment } from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  YellowBox
} from 'react-native';
import FlashMessage from "react-native-flash-message";

/*********************************************** Import AppNavigator *********************************************/
import AppNavigator from './src/navigation';
import AsyncStorage from '@react-native-community/async-storage';


YellowBox.ignoreWarnings(['Calling `getNode()`']);
YellowBox.ignoreWarnings(['Unable to find module for UIManager']);

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      uuidToken: ''
    };
  }

  /****************************************************** componentDidMount ********************************************************/
  componentDidMount() {

    /************************************ MMID_TOKEN_LOGIN AsyncStorage ***************************************/
    AsyncStorage.getItem("UUID_TOKEN").then(UUIDTOKENStorageRes => {
      console.log(UUIDTOKENStorageRes, '----------------UUIDTOKENStorageRes')
      this.setState({
        uuidToken: UUIDTOKENStorageRes
      })
    })
  }

  render() {
    const { uuidToken } = this.state;
    return (
      <Fragment>
        <StatusBar barStyle="light-content" backgroundColor="#f48120" />
        <AppNavigator screenProps={{ uuidToken: uuidToken }} />
        <FlashMessage position="bottom" />
      </Fragment>
    );
  }
}


export default App;
