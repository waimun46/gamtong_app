import React, { Component } from 'react';
import {
  StyleSheet, Text, Dimensions, View, TouchableOpacity, ImageBackground, ScrollView, Image, Button, Modal,
  ActivityIndicator
} from 'react-native';

/*********************************************** React Navigation *********************************************/
import { createAppContainer, NavigationActions, StackActions, SafeAreaView, createSwitchNavigator } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import { createDrawerNavigator, DrawerNavigatorItems, DrawerActions } from 'react-navigation-drawer';
import { createBottomTabNavigator, createMaterialTopTabNavigator } from 'react-navigation-tabs';

/*********************************************** Import Icon *********************************************/
import MAT from 'react-native-vector-icons/MaterialIcons';
import FTT from 'react-native-vector-icons/Fontisto';
import ETY from 'react-native-vector-icons/Entypo';
import ANT from 'react-native-vector-icons/AntDesign';
import EVI from 'react-native-vector-icons/EvilIcons';


/*********************************************** Import Page *********************************************/
import HomePage from '../components/home';
import CouponPage from '../components/coupon';
import BarCode from '../components/coupon/barcode';
import LocationPage from '../components/location';
import InboxPage from '../components/inbox';
import InboxContent from '../components/inbox/inboxContent';
import AboutUs from '../components/about';
import CatagoryPage from '../components/category';
import ProductPage from '../components/product';
import ProductDetails from '../components/product/productDetails';
import AddToCart from '../components/addToCart';
import CheckOut from '../components/addToCart/checkOut';
import PaymentMethod from '../components/addToCart/paymentMethod';
import BrandsPage from '../components/brands';
import Loginpage from '../components/login';
import RegisterPage from '../components/register';
import ProfilePage from '../components/profile';
import EditAccount from '../components/profile/editAccount';
import ForgotPass from '../components/forgetPass';
import Order from '../components/order';
import ResetPass from '../components/profile/resetPass';
import DeliveryStatus from '../components/status';
import WishlistPage from '../components/wishlish';
import VideoGallery from '../components/video';
import ContactPage from '../components/contact';
import SmsLogin from '../components/login/smsLogin';
import SmsVerifly from '../components/login/smsVerifly';
import JoinMember from '../components/member';
import GetOtpPage from '../components/member/getotp';
import OrdinaryInfor from '../components/ordinary';
import PaymentStatusScreen from '../components/paymentStatus';
import CreditInfor from '../components/profile/creditInfor';
import ToShipScreen from '../components/myorder/toShip';
import ToReceiveScreen from '../components/myorder/toReceive';
import ToReviewScreen from '../components/myorder/toReview';
import PostReview from '../components/postReview';
import ReviewScreen from '../components/postReview/review';
import GPoints from '../components/profile/gpoint';
import CreditWallet from '../components/profile/creditsWallet';
import EwalletTopUp from '../components/profile/creditsWallet/topupPage';
import GPointQRCodeScan from '../components/profile/gpoint/qrcode';
import EwalletQRCodeScan from '../components/profile/creditsWallet/qrcode';

import logo from '../assest/logo/logo1.png';

/*********************************************** StyleSheet *********************************************/
const styles = StyleSheet.create({
  menustyWarp: { justifyContent: 'center', flex: 1, paddingLeft: 10, paddingRight: 100, height: '100%', },
  menusty: { color: '#fff', },
  cartsty: { color: '#fff', paddingRight: 10 },
  menustyClose: { color: '#000', paddingRight: 10, },
  closeWarp: { height: 40, justifyContent: "center", alignItems: "flex-start", },
  closeText: { color: '#000', fontSize: 15, padding: 10 },
});

/*********************************************** DrawerButton *********************************************/
const DrawerButton = (props) => {
  return (
    <View>
      <TouchableOpacity onPress={props.navigation.toggleDrawer} style={styles.menustyWarp}>
        <MAT name="menu" size={25} style={styles.menusty} />
      </TouchableOpacity>
    </View>
  );
};




/*********************************************** CartButton *********************************************/
const CartButton = (props, screenProps) => {
  return (
    <View style={{ flexDirection: 'row' }}>

      {
        props.screenProps.uuidToken === null ?
          <TouchableOpacity style={{ paddingLeft: 10, paddingRight: 0 }} onPress={() => props.navigation.navigate('login')}>
            <EVI name="user" size={34} style={styles.cartsty} />
          </TouchableOpacity>
          :
          <TouchableOpacity style={{ paddingLeft: 10, paddingRight: 0 }} onPress={() => props.navigation.navigate('profile')}>
            <EVI name="user" size={34} style={styles.cartsty} />
          </TouchableOpacity>
      }

      <TouchableOpacity style={{ paddingLeft: 3 }} onPress={() => props.navigation.navigate('addToCart')}>
        <ANT name="shoppingcart" size={25} style={styles.cartsty} />
      </TouchableOpacity>
    </View>
  );
};

/*********************************************** createMaterialTopTabNavigator *********************************************/
const MyOrderList = createMaterialTopTabNavigator({
  to_ship: {
    screen: ToShipScreen,
    navigationOptions: {
      title: 'To Ship',
    }
  },
  to_receive: {
    screen: ToReceiveScreen,
    navigationOptions: {
      title: 'To Receive',
    }
  },
  to_review: {
    screen: ToReviewScreen,
    navigationOptions: {
      title: 'To Review',
    }
  },

}, {
    defaultNavigationOptions: ({ navigation }) => {
      return {
        tabBarOptions: {
          style: {
            backgroundColor: 'white',
            elevation: 0, // remove shadow on Android
            shadowOpacity: 0, // remove shadow on iOS,
            borderWidth: 1,
            borderColor: '#ccc',
          },
          activeTintColor: '#f48120',
          inactiveTintColor: '#ccc',
          labelStyle: {
            // fontWeight: 'bold'
          },
          indicatorStyle: {
            backgroundColor: '#f48120',
          },
          // scrollEnabled: true,
        },
      };
    }
  });

/*********************************************** createStackNavigator *********************************************/
const profileStack = createStackNavigator({
  profile: {
    screen: ProfilePage,
    navigationOptions: ({ navigation, screenProps, props }) => ({
      headerLeft: () => <DrawerButton navigation={navigation} />,
      headerRight: () => <CartButton navigation={navigation} screenProps={screenProps} />,
      title: 'My Account',
      headerTitleAlign: 'center',
    })
  },
  gpoint_qr_scan: {
    screen: GPointQRCodeScan,
    navigationOptions: ({ navigation, screenProps, props }) => ({
      title: 'Your QR Code',
      headerTitleAlign: 'center',
    })
  },
  ewallet_qr_scan: {
    screen: EwalletQRCodeScan,
    navigationOptions: ({ navigation, screenProps, props }) => ({
      title: 'Your QR Code',
      headerTitleAlign: 'center',
    })
  },
  credit_wallet: {
    screen: CreditWallet,
    navigationOptions: ({ navigation, screenProps, props }) => ({
      title: 'My Credits',
      headerTitleAlign: 'center',
    })
  },
  ewallet_topup: {
    screen: EwalletTopUp,
    navigationOptions: ({ navigation, screenProps, props }) => ({
      title: 'Credit TopUp',
      headerTitleAlign: 'center',
    })
  },
  g_points: {
    screen: GPoints,
    navigationOptions: ({ navigation, screenProps, props }) => ({
      title: 'G Points',
      headerTitleAlign: 'center',
    })
  },
  credit_infor: {
    screen: CreditInfor,
    navigationOptions: ({ navigation, screenProps, props }) => ({
      title: 'My Credit Info',
      headerTitleAlign: 'center',
    })
  },
  edit_acc: {
    screen: EditAccount,
    navigationOptions: ({ navigation, screenProps, props }) => ({
      title: 'Edit Account',
      headerTitleAlign: 'center',
    })
  },
  reset_pass: {
    screen: ResetPass,
    navigationOptions: ({ navigation, screenProps, props }) => ({
      title: 'Reset Password',
      headerTitleAlign: 'center',
    })
  },
  member: {
    screen: JoinMember,
    navigationOptions: ({ navigation, screenProps, props }) => ({
      title: 'Join Member',
      headerTitleAlign: 'center',
    })
  },
  otp: {
    screen: GetOtpPage,
    navigationOptions: ({ navigation, screenProps, props }) => ({
      title: 'Get OTP',
      headerTitleAlign: 'center',
    })
  },
  ewallet_topup: {
    screen: EwalletTopUp,
    navigationOptions: ({ navigation, screenProps, props }) => ({
      title: 'Credit TopUp',
      headerTitleAlign: 'center',
    })
  },
  ordinary_Infor: {
    screen: OrdinaryInfor,
    navigationOptions: ({ navigation, screenProps, props }) => ({
      title: 'Ordinary Info',
      headerTitleAlign: 'center',
    })
  },
  payment_status: {
    screen: PaymentStatusScreen,
    navigationOptions: ({ navigation, screenProps, props }) => ({
      title: 'Payment Success',
      headerTitleAlign: 'center',
    })
  },
  wishlist: {
    screen: WishlistPage,
    navigationOptions: ({ navigation, screenProps, props }) => ({
      title: 'My Wishlist',
      headerTitleAlign: 'center',
    })
  },
  delivery: {
    screen: DeliveryStatus,
    navigationOptions: ({ navigation, screenProps, props }) => ({
      title: 'Track Package',
      headerTitleAlign: 'center',
    })
  },
  my_order: {
    screen: MyOrderList,
    navigationOptions: ({ navigation, screenProps, props }) => ({
      title: 'My Purchases',
      headerTitleAlign: 'center',
    })
  },
  post_review: {
    screen: PostReview,
    navigationOptions: ({ navigation, screenProps, props }) => ({
      title: 'Post Review',
      headerTitleAlign: 'center',
    })
  },
  review: {
    screen: ReviewScreen,
    navigationOptions: ({ navigation, screenProps, props }) => ({
      title: 'Review',
      headerTitleAlign: 'center',
    })
  },


}, {
    headerTitleAlign: 'center',
    defaultNavigationOptions: {
      headerStyle: { backgroundColor: '#f48120', },
      headerTintColor: '#fff',
      headerTitleStyle: { fontWeight: 'bold' },
      headerBackTitle: '   ',
    }
  })


/********************************** Home *************************************/
const Home = createStackNavigator({

  home: {
    screen: HomePage,
    navigationOptions: ({ navigation, screenProps, props }) => ({
      headerLeft: () => <DrawerButton navigation={navigation} />,
      headerRight: () => <CartButton navigation={navigation} screenProps={screenProps} />,
      title: 'Home',
      headerTitleAlign: 'center',
    })
  },
  login: {
    screen: Loginpage,
    navigationOptions: ({ navigation, screenProps, props }) => ({
      title: ' ',
      headerTitleAlign: 'center',
    })
  },
  sms: {
    screen: SmsLogin,
    navigationOptions: ({ navigation, screenProps, props }) => ({
      title: 'SMS Login',
      headerTitleAlign: 'center',
    })
  },
  sms_verifly: {
    screen: SmsVerifly,
    navigationOptions: ({ navigation, screenProps, props }) => ({
      title: 'SMS Verifly',
      headerTitleAlign: 'center',
    })
  },
  member: {
    screen: JoinMember,
    navigationOptions: ({ navigation, screenProps, props }) => ({
      title: 'Join Member',
      headerTitleAlign: 'center',
    })
  },

  profile: {
    screen: ProfilePage,
    navigationOptions: ({ navigation, screenProps, props }) => ({
      title: 'My Account',
      headerTitleAlign: 'center',
    })
  },
  gpoint_qr_scan: {
    screen: GPointQRCodeScan,
    navigationOptions: ({ navigation, screenProps, props }) => ({
      title: 'Your QR Code',
      headerTitleAlign: 'center',
    })
  },
  ewallet_qr_scan: {
    screen: EwalletQRCodeScan,
    navigationOptions: ({ navigation, screenProps, props }) => ({
      title: 'Your QR Code',
      headerTitleAlign: 'center',
    })
  },
  credit_wallet: {
    screen: CreditWallet,
    navigationOptions: ({ navigation, screenProps, props }) => ({
      title: 'My Credits',
      headerTitleAlign: 'center',
    })
  },
  ewallet_topup: {
    screen: EwalletTopUp,
    navigationOptions: ({ navigation, screenProps, props }) => ({
      title: 'Credit TopUp',
      headerTitleAlign: 'center',
    })
  },
  g_points: {
    screen: GPoints,
    navigationOptions: ({ navigation, screenProps, props }) => ({
      title: 'G Points',
      headerTitleAlign: 'center',
    })
  },
  credit_infor: {
    screen: CreditInfor,
    navigationOptions: ({ navigation, screenProps, props }) => ({
      title: 'My Credit Info',
      headerTitleAlign: 'center',
    })
  },
  edit_acc: {
    screen: EditAccount,
    navigationOptions: ({ navigation, screenProps, props }) => ({
      title: 'Edit Account',
      headerTitleAlign: 'center',
    })
  },
  reset_pass: {
    screen: ResetPass,
    navigationOptions: ({ navigation, screenProps, props }) => ({
      title: 'Reset Password',
      headerTitleAlign: 'center',
    })
  },
  my_order: {
    screen: MyOrderList,
    navigationOptions: ({ navigation, screenProps, props }) => ({
      title: 'My Purchases',
      headerTitleAlign: 'center',
    })
  },
  post_review: {
    screen: PostReview,
    navigationOptions: ({ navigation, screenProps, props }) => ({
      title: 'Post Review',
      headerTitleAlign: 'center',
    })
  },
  review: {
    screen: ReviewScreen,
    navigationOptions: ({ navigation, screenProps, props }) => ({
      title: 'Review',
      headerTitleAlign: 'center',
    })
  },

  wishlist: {
    screen: WishlistPage,
    navigationOptions: ({ navigation, screenProps, props }) => ({
      title: 'My Wishlist',
      headerTitleAlign: 'center',
    })
  },
  catagory: {
    screen: CatagoryPage,
    navigationOptions: ({ navigation, screenProps, props }) => ({
      headerRight: () => <CartButton navigation={navigation} screenProps={screenProps} />,
      title: 'Catagory',
      headerTitleAlign: 'center',
    })
  },
  brands: {
    screen: BrandsPage,
    navigationOptions: ({ navigation, screenProps, props }) => ({
      headerRight: () => <CartButton navigation={navigation} screenProps={screenProps} />,
      title: 'Catagory',
      headerTitleAlign: 'center',
    })
  },
  product: {
    screen: ProductPage,
    navigationOptions: ({ navigation, screenProps, props }) => ({
      headerRight: () => <CartButton navigation={navigation} screenProps={screenProps} />,
      title: 'Product',
      headerTitleAlign: 'center',
    })
  },
  productDetails: {
    screen: ProductDetails,
    navigationOptions: ({ navigation, screenProps, props }) => ({
      // headerRight: () => <CartButton navigation={navigation} screenProps={screenProps} />,
      // title: 'Item',
      // headerTitleAlign: 'center',
      headerShown: false,
    })
  },
  ordinary_Infor: {
    screen: OrdinaryInfor,
    navigationOptions: ({ navigation, screenProps, props }) => ({
      title: 'Ordinary Info',
      headerTitleAlign: 'center',
    })
  },

  otp: {
    screen: GetOtpPage,
    navigationOptions: ({ navigation, screenProps, props }) => ({
      title: 'Get OTP',
      headerTitleAlign: 'center',
    })
  },

  payment_status: {
    screen: PaymentStatusScreen,
    navigationOptions: ({ navigation, screenProps, props }) => ({
      title: 'Payment Success',
      headerTitleAlign: 'center',
    })
  },
  addToCart: {
    screen: AddToCart,
    navigationOptions: ({ navigation, screenProps, props }) => ({
      title: 'Add To Cart',
      headerTitleAlign: 'center',
    })
  },

  register: {
    screen: RegisterPage,
    navigationOptions: ({ navigation, screenProps, props }) => ({
      title: 'Sign up',
      headerTitleAlign: 'center',
    })
  },
  forgotPass: {
    screen: ForgotPass,
    navigationOptions: ({ navigation, screenProps, props }) => ({
      title: 'Forgot Password',
      headerTitleAlign: 'center',
    })
  },
  edit_acc: {
    screen: EditAccount,
    navigationOptions: ({ navigation, screenProps, props }) => ({
      title: 'Edit Account',
      headerTitleAlign: 'center',
    })
  },
  reset_pass: {
    screen: ResetPass,
    navigationOptions: ({ navigation, screenProps, props }) => ({
      title: 'Reset Password',
      headerTitleAlign: 'center',
    })
  },
  order: {
    screen: Order,
    navigationOptions: ({ navigation, screenProps, props }) => ({
      title: 'My Order',
      headerTitleAlign: 'center',
    })
  },
  checkout: {
    screen: CheckOut,
    navigationOptions: ({ navigation, screenProps, props }) => ({
      title: 'Confirm Order',
      headerTitleAlign: 'center',
    })
  },
  payment_method: {
    screen: PaymentMethod,
    navigationOptions: ({ navigation, screenProps, props }) => ({
      title: 'Payment Methods',
      headerTitleAlign: 'center',
    })
  },
  coupon: {
    screen: CouponPage,
    navigationOptions: ({ navigation, screenProps, props }) => ({
      title: 'My Vouchers',
      headerTitleAlign: 'center',
    })
  },
  barcode: {
    screen: BarCode,
    navigationOptions: ({ navigation, screenProps, props }) => ({
      title: 'Barcode',
      headerTitleAlign: 'center',
      headerBackTitle: '  ',
    })
  },
  delivery: {
    screen: DeliveryStatus,
    navigationOptions: ({ navigation, screenProps, props }) => ({
      title: 'Track Package',
      headerTitleAlign: 'center',
    })
  },
  credit_infor: {
    screen: CreditInfor,
    navigationOptions: ({ navigation, screenProps, props }) => ({
      title: 'My Credit Info',
      headerTitleAlign: 'center',
    })
  },


}, {
    headerTitleAlign: 'center',
    defaultNavigationOptions: {
      headerStyle: { backgroundColor: '#f48120', },
      headerTintColor: '#fff',
      headerTitleStyle: { fontWeight: 'bold' },
      headerBackTitle: '   ',
    }
  })

/********************************** About Us *************************************/
const About = createStackNavigator({
  about: {
    screen: AboutUs,
    navigationOptions: ({ navigation, screenProps, props }) => ({
      headerLeft: () => <DrawerButton navigation={navigation} />,
      headerRight: () => <CartButton navigation={navigation} screenProps={screenProps} />,
      title: 'About Us',
      headerTitleAlign: 'center',
    })
  },
  addToCart: {
    screen: AddToCart,
    navigationOptions: ({ navigation, screenProps, props }) => ({
      title: 'Add To Cart',
      headerTitleAlign: 'center',
    })
  },
  register: {
    screen: RegisterPage,
    navigationOptions: ({ navigation, screenProps, props }) => ({
      title: 'Sign up',
      headerTitleAlign: 'center',
    })
  },
  forgotPass: {
    screen: ForgotPass,
    navigationOptions: ({ navigation, screenProps, props }) => ({
      title: 'Forgot Password',
      headerTitleAlign: 'center',
    })
  },
  profile: {
    screen: ProfilePage,
    navigationOptions: ({ navigation, screenProps, props }) => ({
      title: 'My Account',
      headerTitleAlign: 'center',
    })
  },
  reset_pass: {
    screen: ResetPass,
    navigationOptions: ({ navigation, screenProps, props }) => ({
      title: 'Reset Password',
      headerTitleAlign: 'center',
    })
  },
  edit_acc: {
    screen: EditAccount,
    navigationOptions: ({ navigation, screenProps, props }) => ({
      title: 'Edit Account',
      headerTitleAlign: 'center',
    })
  },
  order: {
    screen: Order,
    navigationOptions: ({ navigation, screenProps, props }) => ({
      title: 'My Order',
      headerTitleAlign: 'center',
    })
  },
  checkout: {
    screen: CheckOut,
    navigationOptions: ({ navigation, screenProps, props }) => ({
      title: 'Confirm Order',
      headerTitleAlign: 'center',
    })
  },
  payment_method: {
    screen: PaymentMethod,
    navigationOptions: ({ navigation, screenProps, props }) => ({
      title: 'Payment Methods',
      headerTitleAlign: 'center',
    })
  },
  member: {
    screen: JoinMember,
    navigationOptions: ({ navigation, screenProps, props }) => ({
      title: 'Join Member',
      headerTitleAlign: 'center',
    })
  },
  otp: {
    screen: GetOtpPage,
    navigationOptions: ({ navigation, screenProps, props }) => ({
      title: 'Get OTP',
      headerTitleAlign: 'center',
    })
  },
  ordinary_Infor: {
    screen: OrdinaryInfor,
    navigationOptions: ({ navigation, screenProps, props }) => ({
      title: 'Ordinary Info',
      headerTitleAlign: 'center',
    })
  },
  payment_status: {
    screen: PaymentStatusScreen,
    navigationOptions: ({ navigation, screenProps, props }) => ({
      title: 'Payment Success',
      headerTitleAlign: 'center',
    })
  },
  credit_infor: {
    screen: CreditInfor,
    navigationOptions: ({ navigation, screenProps, props }) => ({
      title: 'My Credit Info',
      headerTitleAlign: 'center',
    })
  },
}, {
    defaultNavigationOptions: {
      headerStyle: { backgroundColor: '#f48120', },
      headerTintColor: '#fff',
      headerTitleStyle: { fontWeight: 'bold' },
      headerBackTitle: '   ',
    }
  })

/********************************** Branchers *************************************/
const Brancher = createStackNavigator({
  brancher: {
    screen: LocationPage,
    navigationOptions: ({ navigation, screenProps, props }) => ({
      headerLeft: () => <DrawerButton navigation={navigation} />,
      headerRight: () => <CartButton navigation={navigation} screenProps={screenProps} />,
      title: 'Branchers',
      headerTitleAlign: 'center',
    })
  },
  addToCart: {
    screen: AddToCart,
    navigationOptions: ({ navigation, screenProps, props }) => ({
      title: 'Add To Cart',
      headerTitleAlign: 'center',
    })
  },
  register: {
    screen: RegisterPage,
    navigationOptions: ({ navigation, screenProps, props }) => ({
      title: 'Sign up',
      headerTitleAlign: 'center',
    })
  },
  forgotPass: {
    screen: ForgotPass,
    navigationOptions: ({ navigation, screenProps, props }) => ({
      title: 'Forgot Password',
      headerTitleAlign: 'center',
    })
  },
  profile: {
    screen: ProfilePage,
    navigationOptions: ({ navigation, screenProps, props }) => ({
      title: 'My Account',
      headerTitleAlign: 'center',
    })
  },
  reset_pass: {
    screen: ResetPass,
    navigationOptions: ({ navigation, screenProps, props }) => ({
      title: 'Reset Password',
      headerTitleAlign: 'center',
    })
  },
  edit_acc: {
    screen: EditAccount,
    navigationOptions: ({ navigation, screenProps, props }) => ({
      title: 'Edit Account',
      headerTitleAlign: 'center',
    })
  },
  order: {
    screen: Order,
    navigationOptions: ({ navigation, screenProps, props }) => ({
      title: 'My Order',
      headerTitleAlign: 'center',
    })
  },
  checkout: {
    screen: CheckOut,
    navigationOptions: ({ navigation, screenProps, props }) => ({
      title: 'Confirm Order',
      headerTitleAlign: 'center',
    })
  },
  payment_method: {
    screen: PaymentMethod,
    navigationOptions: ({ navigation, screenProps, props }) => ({
      title: 'Payment Methods',
      headerTitleAlign: 'center',
    })
  },
  member: {
    screen: JoinMember,
    navigationOptions: ({ navigation, screenProps, props }) => ({
      title: 'Join Member',
      headerTitleAlign: 'center',
    })
  },
  otp: {
    screen: GetOtpPage,
    navigationOptions: ({ navigation, screenProps, props }) => ({
      title: 'Get OTP',
      headerTitleAlign: 'center',
    })
  },
  ordinary_Infor: {
    screen: OrdinaryInfor,
    navigationOptions: ({ navigation, screenProps, props }) => ({
      title: 'Ordinary Info',
      headerTitleAlign: 'center',
    })
  },
  payment_status: {
    screen: PaymentStatusScreen,
    navigationOptions: ({ navigation, screenProps, props }) => ({
      title: 'Payment Success',
      headerTitleAlign: 'center',
    })
  },
  credit_infor: {
    screen: CreditInfor,
    navigationOptions: ({ navigation, screenProps, props }) => ({
      title: 'My Credit Info',
      headerTitleAlign: 'center',
    })
  },
}, {
    defaultNavigationOptions: {
      headerStyle: { backgroundColor: '#f48120', },
      headerTintColor: '#fff',
      headerTitleStyle: { fontWeight: 'bold' },
      headerBackTitle: '   ',
    }
  })

/********************************** VideoGallery *************************************/
const Video = createStackNavigator({
  video: {
    screen: VideoGallery,
    navigationOptions: ({ navigation, screenProps, props }) => ({
      headerLeft: () => <DrawerButton navigation={navigation} />,
      headerRight: () => <CartButton navigation={navigation} screenProps={screenProps} />,
      title: 'Video Gallery',
      headerTitleAlign: 'center',
    })
  },
  addToCart: {
    screen: AddToCart,
    navigationOptions: ({ navigation, screenProps, props }) => ({
      title: 'Add To Cart',
      headerTitleAlign: 'center',
    })
  },
  register: {
    screen: RegisterPage,
    navigationOptions: ({ navigation, screenProps, props }) => ({
      title: 'Sign up',
      headerTitleAlign: 'center',
    })
  },
  forgotPass: {
    screen: ForgotPass,
    navigationOptions: ({ navigation, screenProps, props }) => ({
      title: 'Forgot Password',
      headerTitleAlign: 'center',
    })
  },
  profile: {
    screen: ProfilePage,
    navigationOptions: ({ navigation, screenProps, props }) => ({
      title: 'My Account',
      headerTitleAlign: 'center',
    })
  },
  reset_pass: {
    screen: ResetPass,
    navigationOptions: ({ navigation, screenProps, props }) => ({
      title: 'Reset Password',
      headerTitleAlign: 'center',
    })
  },
  edit_acc: {
    screen: EditAccount,
    navigationOptions: ({ navigation, screenProps, props }) => ({
      title: 'Edit Account',
      headerTitleAlign: 'center',
    })
  },
  order: {
    screen: Order,
    navigationOptions: ({ navigation, screenProps, props }) => ({
      title: 'My Order',
      headerTitleAlign: 'center',
    })
  },
  checkout: {
    screen: CheckOut,
    navigationOptions: ({ navigation, screenProps, props }) => ({
      title: 'Confirm Order',
      headerTitleAlign: 'center',
    })
  },
  payment_method: {
    screen: PaymentMethod,
    navigationOptions: ({ navigation, screenProps, props }) => ({
      title: 'Payment Methods',
      headerTitleAlign: 'center',
    })
  },
  member: {
    screen: JoinMember,
    navigationOptions: ({ navigation, screenProps, props }) => ({
      title: 'Join Member',
      headerTitleAlign: 'center',
    })
  },
  otp: {
    screen: GetOtpPage,
    navigationOptions: ({ navigation, screenProps, props }) => ({
      title: 'Get OTP',
      headerTitleAlign: 'center',
    })
  },
  ordinary_Infor: {
    screen: OrdinaryInfor,
    navigationOptions: ({ navigation, screenProps, props }) => ({
      title: 'Ordinary Info',
      headerTitleAlign: 'center',
    })
  },
  payment_status: {
    screen: PaymentStatusScreen,
    navigationOptions: ({ navigation, screenProps, props }) => ({
      title: 'Payment Success',
      headerTitleAlign: 'center',
    })
  },
  credit_infor: {
    screen: CreditInfor,
    navigationOptions: ({ navigation, screenProps, props }) => ({
      title: 'My Credit Info',
      headerTitleAlign: 'center',
    })
  },
}, {
    defaultNavigationOptions: {
      headerStyle: { backgroundColor: '#f48120', },
      headerTintColor: '#fff',
      headerTitleStyle: { fontWeight: 'bold' },
      headerBackTitle: '   ',
    }
  })


/********************************** Contact *************************************/
const Contact = createStackNavigator({
  contact: {
    screen: ContactPage,
    navigationOptions: ({ navigation, screenProps, props }) => ({
      headerLeft: () => <DrawerButton navigation={navigation} />,
      headerRight: () => <CartButton navigation={navigation} screenProps={screenProps} />,
      title: 'Contact Us',
      headerTitleAlign: 'center',
    })
  },
  addToCart: {
    screen: AddToCart,
    navigationOptions: ({ navigation, screenProps, props }) => ({
      title: 'Add To Cart',
      headerTitleAlign: 'center',
    })
  },
  register: {
    screen: RegisterPage,
    navigationOptions: ({ navigation, screenProps, props }) => ({
      title: 'Sign up',
      headerTitleAlign: 'center',
    })
  },
  forgotPass: {
    screen: ForgotPass,
    navigationOptions: ({ navigation, screenProps, props }) => ({
      title: 'Forgot Password',
      headerTitleAlign: 'center',
    })
  },
  profile: {
    screen: ProfilePage,
    navigationOptions: ({ navigation, screenProps, props }) => ({
      title: 'My Account',
      headerTitleAlign: 'center',
    })
  },
  reset_pass: {
    screen: ResetPass,
    navigationOptions: ({ navigation, screenProps, props }) => ({
      title: 'Reset Password',
      headerTitleAlign: 'center',
    })
  },
  edit_acc: {
    screen: EditAccount,
    navigationOptions: ({ navigation, screenProps, props }) => ({
      title: 'Edit Account',
      headerTitleAlign: 'center',
    })
  },
  order: {
    screen: Order,
    navigationOptions: ({ navigation, screenProps, props }) => ({
      title: 'My Order',
      headerTitleAlign: 'center',
    })
  },
  checkout: {
    screen: CheckOut,
    navigationOptions: ({ navigation, screenProps, props }) => ({
      title: 'Confirm Order',
      headerTitleAlign: 'center',
    })
  },
  payment_method: {
    screen: PaymentMethod,
    navigationOptions: ({ navigation, screenProps, props }) => ({
      title: 'Payment Methods',
      headerTitleAlign: 'center',
    })
  },
  member: {
    screen: JoinMember,
    navigationOptions: ({ navigation, screenProps, props }) => ({
      title: 'Join Member',
      headerTitleAlign: 'center',
    })
  },
  otp: {
    screen: GetOtpPage,
    navigationOptions: ({ navigation, screenProps, props }) => ({
      title: 'Get OTP',
      headerTitleAlign: 'center',
    })
  },
  ordinary_Infor: {
    screen: OrdinaryInfor,
    navigationOptions: ({ navigation, screenProps, props }) => ({
      title: 'Ordinary Info',
      headerTitleAlign: 'center',
    })
  },
  payment_status: {
    screen: PaymentStatusScreen,
    navigationOptions: ({ navigation, screenProps, props }) => ({
      title: 'Payment Success',
      headerTitleAlign: 'center',
    })
  },
  credit_infor: {
    screen: CreditInfor,
    navigationOptions: ({ navigation, screenProps, props }) => ({
      title: 'My Credit Info',
      headerTitleAlign: 'center',
    })
  },
}, {
    defaultNavigationOptions: {
      headerStyle: { backgroundColor: '#f48120', },
      headerTintColor: '#fff',
      headerTitleStyle: { fontWeight: 'bold' },
      headerBackTitle: '   ',
    }
  })




////////////////////////////////////////////////////////////// CustomDrawerComponent ////////////////////////////////////////////////////////////// 
const CustomDrawerComponent = (props, screenProps) => {
  // console.log(props, 'props-navi')
  // console.log(props.screenProps.mmidToken, 'mmidToken-navi')

  // let profileData = props.screenProps.profile || [];
  // //console.log(profileData, 'profile')

  // function logoutFuc() {
  //   //console.log('logoutFuc-navi')
  //   AsyncStorage.removeItem('MMID_TOKEN_LOGIN');
  //   props.navigation.navigate('home', RNRestart.Restart());
  //   props.navigation.dispatch(DrawerActions.closeDrawer())
  // }

  return (
    // <SafeAreaProvider >
    // <SafeAreaView style={{ flex: 1 }} forceInset={{ top: 'never', bottom: 'never' }}>
    <SafeAreaView forceInset={{ top: 'never', bottom: 'never' }}>
      <View style={{ height: '100%' }}>
        <View style={styles.closeWarp}>
          <TouchableOpacity onPress={props.navigation.closeDrawer} style={{ flexDirection: 'row', width: '100%', justifyContent: 'flex-end' }}>
            <ANT name="close" size={30} style={styles.menustyClose} />
            {/* <Text style={styles.closeText}>Close</Text> */}
          </TouchableOpacity>
        </View>
        <View style={{ alignItems: 'center', marginBottom: 10, }}>
          <View style={{ width: '80%', height: 80 }}>
            <Image source={logo} style={{ width: '100%', height: '100%', resizeMode: 'contain' }} />
          </View>
        </View>
        <ScrollView style={{ backgroundColor: '#f48120' }}>
          <DrawerNavigatorItems {...props}
            onItemPress={({ route, focused }) => {
              if (!focused) {
                setTimeout(() => {
                  props.navigation.navigate(route.routeName)
                }, 0)
              }
              // props.navigation.navigate('DrawerClose');
            }}
          />
        </ScrollView>
        {/* {
          props.screenProps.mmidToken === null ? (
            null
          ) : (
              <View style={{ justifyContent: 'flex-end', flex: 1 }}>
                <View style={[styles.logoutWarp,]}>
                  <View style={{ width: '70%', paddingLeft: 30 }}>
                    <TouchableOpacity onPress={logoutFuc}>
                      <Text style={{ color: '#fff', fontSize: 25, marginBottom: 5 }} >Logout</Text>
                    </TouchableOpacity>
                    <Text style={{ color: '#ea8c8c', fontWeight: 'bold' }}>{profileData.name}</Text>
                  </View>
                  <View style={{ width: '30%' }}>
                    {
                      profileData.photo === "" || profileData.photo === null ?
                        <Image source={emptyProfile} style={{ width: '100%', height: '100%' }} />
                        :
                        <Image source={{ uri: profileData.photo }} style={{ width: '100%', height: '100%' }} />
                    }
                  </View>
                </View>
              </View>
            )
        } */}
      </View>
      {/* </ImageBackground > */}
    </SafeAreaView>
    // </SafeAreaProvider>

  )

};


/*********************************************** createDrawerNavigator *********************************************/
const AppDrawerNavigator = createDrawerNavigator({
  /********************************** Home *************************************/
  home: {
    name: 'Home',
    screen: Home,
    navigationOptions: () => ({
      drawerLabel: 'Home',
    })
  },
  /********************************** About Us *************************************/
  about: {
    name: 'About Us',
    screen: About,
    navigationOptions: () => ({
      drawerLabel: 'About Us',
    })
  },
  /********************************** Branchers *************************************/
  brancher: {
    name: 'Branchers',
    screen: Brancher,
    navigationOptions: () => ({
      drawerLabel: 'Branchers',
    })
  },
  /********************************** Contact *************************************/
  contact: {
    name: 'Contact',
    screen: Contact,
    navigationOptions: () => ({
      drawerLabel: 'Contact Us',
    })
  },
  /********************************** Video *************************************/
  video: {
    name: 'Video',
    screen: Video,
    navigationOptions: () => ({
      drawerLabel: 'Video Gallery',
    })
  },

  // account: {
  //   name: 'Account',
  //   screen: profileStack,
  //   navigationOptions: () => ({
  //     drawerLabel: 'My Account',
  //   })
  // }

}, {
    initialRouteName: 'home',
    contentComponent: props => <CustomDrawerComponent {...props} />,
    drawerType: 'front',
    drawerBackgroundColor: '#fff',
    drawerWidth: '80%',
    overlayColor: 'rgba(0, 0, 0, 0.9)',
    drawerPosition: 'left',
    // drawerLockMode: "unlocked",
    // disableGestures: false,
    contentOptions: {
      labelStyle: { color: '#fff', fontSize: 20, fontWeight: 'normal', paddingLeft: 20, },
      activeLabelStyle: { color: 'yellow' },
      // itemsContainerStyle: { marginTop: 30, },
      itemStyle: { borderBottomWidth: .5, borderBottomColor: '#ffffff2e', }
    }
  });

/*********************************************** createSwitchNavigator *********************************************/
const AppNavigator = createSwitchNavigator({
  //  Welcome: { screen: WelcomeScreen },
  Dashboard: { screen: AppDrawerNavigator }
});






export default createAppContainer(AppNavigator);