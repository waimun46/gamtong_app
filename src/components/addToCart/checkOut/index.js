import React, { Component } from 'react';
import { View, StyleSheet, ScrollView, TouchableOpacity, Image } from 'react-native';
import { Card, CardItem, Text, Body, Button, Picker } from 'native-base';
import ANT from 'react-native-vector-icons/AntDesign';
import mylogo from '../../../assest/maybank.jpg'


class CheckOut extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selected: "key1"
    };
  }

  onValueChange(value: string) {
    this.setState({
      selected: value
    });
  }

  render() {
    const navigateActions = this.props.navigation.navigate;

    return (
      <View style={styles.container}>
        <ScrollView>
          <View style={{
            marginBottom: 0, marginTop: 10, flexDirection: 'row',
            justifyContent: 'space-between', paddingLeft: 10, paddingRight: 10,
          }}>
            <Text note >Date: 20-4-2020 </Text>
            <Text note >Time: 12.00 PM</Text>
          </View>
          <View style={{ paddingTop: 10, paddingBottom: 5 }}>
            <View style={{ backgroundColor: '#fff', padding: 20 }}>
              <Text note style={{ fontWeight: 'bold', color: '#000' }}>Personal Information</Text>
              <Text note style={{ marginTop: 5, }}>John Tan</Text>
              <Text note style={{ marginTop: 5, }}>012 3456789</Text>
              <Text note style={{ marginTop: 5, }}>
                G1.PT.02 Sunway Pyramid, 3, Jalan PJS 11/15,
                Bandar Sunway, 47500 Subang Jaya,Selangor
                </Text>
            </View>
          </View>

          <View style={{ paddingTop: 5, paddingBottom: 5 }}>

            <View style={{ backgroundColor: '#fff', padding: 20, paddingTop: 10 }}>
              <Text note style={{ fontWeight: 'bold', color: '#000' }}>Order Details</Text>
              <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', marginTop: 5, }}>
                <Text note style={{}}>KFC   x2</Text>
                <Text note style={{ marginTop: 5 }}>RM 36.50</Text>
              </View>
              <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', marginTop: 5, }}>
                <Text note style={{}}>Delivery Fee</Text>
                <Text note style={{ marginTop: 5 }}>RM 5.00</Text>
              </View>
              <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', marginTop: 5, }}>
                <Text note style={{ color: '#000', }}>Total (include Tax)</Text>
                <Text note style={{ marginTop: 5, color: 'green', fontWeight: 'bold' }}>RM 41.50</Text>
              </View>
            </View>
          </View>

          <View style={{ paddingTop: 5, paddingBottom: 5 }}>
            <View style={{
              backgroundColor: '#fff', paddingLeft: 20, paddingRight: 20, paddingBottom: 10, paddingTop: 10,
              flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center',
            }}>
              <Text note style={{ fontWeight: 'bold', color: '#000' }}>Delivery Time</Text>
              <Picker
                mode="dropdown"
                iosHeader="Select Time"
                headerTitleStyle={{ color: '#fff' }}
                headerStyle={{ backgroundColor: "#f48120" }}
                headerBackButtonTextStyle={{ color: "#fff" }}
                headerBackButtonText={<ANT name="left" size={22} />}
                iosIcon={<ANT name="right" size={20} style={styles.iconsty} />}
                style={{ width: undefined, height: 'auto' }}
                selectedValue={this.state.selected}
                onValueChange={this.onValueChange.bind(this)}
                textStyle={{ fontSize: 14, }}
              >
                <Picker.Item label="45 min" value="key0" />
                <Picker.Item label="1 hour" value="key1" />
              </Picker>
            </View>
          </View>


          <View style={{ paddingTop: 5, paddingBottom: 5 }}>
            <View>
              <TouchableOpacity style={{ backgroundColor: '#fff', padding: 20, flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', }}>
                <Text note style={{ fontWeight: 'bold', color: '#000' }}>Payment Methods</Text>
                <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', }}>
                <Image source={mylogo} style={{ width: 40, height: 30, marginRight: 10 }} />
                  <ANT name="right" size={20} style={styles.iconsty} />
                </View>
              </TouchableOpacity>
              <View style={{ borderTopWidth: .5, borderTopColor: '#eee' }}>
                <View style={{ backgroundColor: '#fff', padding: 20, flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', }}>
                  <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', }}>
                    <Image source={mylogo} style={{ width: 50, height: 35, marginRight: 10 }} />
                    <Text note style={{ marginTop: 5 }}>MayBank</Text>
                  </View>
                  <Text note style={{ marginTop: 5 }}>RM 41.50</Text>
                </View>
              </View>
            </View>
          </View>

          <View style={{ paddingTop: 5, paddingBottom: 5 }}>
            <View style={{ backgroundColor: '#fff', padding: 20, flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', }}>
              <Text note style={{ fontWeight: 'bold', color: '#000' }}>Delivery Status</Text>
              <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', }}>
                <Text note style={{ marginTop: 5 }}>Proseccing</Text>
                <View style={{ backgroundColor: 'red', padding: 5, borderRadius: 50, marginLeft: 10, marginTop: 5 }} />
              </View>

            </View>
          </View>



          <View style={{ padding: 20, marginTop: 20, paddingBottom: 40 }}>
            <Button full dark onPress={() => navigateActions('payment_status')}>
              <Text>CheckOut</Text>
            </Button>
          </View>


        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: { flex: 1 },
  iconsty: { color: '#ccc' }
})

export default CheckOut;
