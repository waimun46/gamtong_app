import React, { Component } from 'react';
import { View, StyleSheet, FlatList, TouchableOpacity, TextInput, ScrollView } from 'react-native';
import { List, ListItem, Thumbnail, Text, Left, Body, Right, Button } from 'native-base';
import NumericInput from 'react-native-numeric-input';
import ANT from 'react-native-vector-icons/AntDesign';




const data = [
  {
    item: 'PRODUCT 1', name: 'This is my products.', price: '40.00', discount: '15%',
    img: require('../../assest/dimsum/11.png'), star: 4.6
  },
  {
    item: 'PRODUCT 2', name: 'This is my products.', price: '35.50', discount: '',
    img: require('../../assest/dimsum/01.png'), star: 3.6
  },
]

class AddToCart extends Component {
  constructor(props) {
    super(props);
    this.state = {
      valueItem: 1
    };
  }

  _renderCart = ({ item, index }) => {
    const navigateActions = this.props.navigation.navigate;
    return (
      <List>
        <ListItem thumbnail style={{marginLeft: 10}} onPress={() => navigateActions('productDetails', { data: item })} >
          <Left style={{ flexDirection: 'column' }}>
            <Thumbnail square source={item.img} style={{width: 80, height: 80}} />
          </Left>
          <Body >
            <Text style={{ marginBottom: 10 }}>{item.item}</Text>
            <Text note>{item.name}</Text>
            <Text style={{ marginTop: 10, fontWeight: 'bold' }}>RM {item.price}</Text>
          </Body>
          <Right>
            <TouchableOpacity style={{ paddingBottom: 10 }}>
              <ANT name="closecircle" color={"red"} size={16} style={{ textAlign: 'right', }} />
            </TouchableOpacity>
            <Text style={{ fontWeight: 'bold', marginBottom: 5, }}>RM {item.price}</Text>
            <NumericInput
              value={this.state.valueItem}
              onChange={(valueItem) => { this.setState({ valueItem }); console.log(valueItem) }}
              onLimitReached={(isMax, msg) => console.log(isMax, msg)}
              totalWidth={120}
              totalHeight={30}
              iconSize={25}
              step={1}
              valueType='real'
              rounded
              textColor='#000'
              borderColor='#ccc'
              iconStyle={{ color: 'white' }}
              rightButtonBackgroundColor='#ccc'
              leftButtonBackgroundColor='#ccc'
              minValue={1}
            // maxValue={9999}
            />
          </Right>
        </ListItem>
      </List>
    )
  }

  render() {
    const navigateActions = this.props.navigation.navigate;

    return (
      <View style={styles.container}>
        <ScrollView>
          <FlatList
            data={data}
            extraData={this.state}
            renderItem={this._renderCart}
            keyExtractor={(item, index) => index.toString()}
          />
          <View style={{ padding: 20, flexDirection: 'row', justifyContent: 'space-between', marginTop: 10, }}>
            <TextInput placeholder='Enter Voucher Code'
              style={{ padding: 10, borderColor: '#ccc', borderWidth: .5, width: '70%', height: 45 }} />
            <View style={{ width: '30%', backgroundColor: '#ccc', }}>
              <TouchableOpacity style={{ height: 45, justifyContent: 'center', alignItems: 'center', }} >
                <Text style={{ color: '#fff' }}>APPLY</Text>
              </TouchableOpacity>
            </View>
          </View>

          <View style={{ padding: 20 }}>
            <View style={{ flexDirection: 'row', justifyContent: 'flex-end' }}>
              <Text style={{ width: '70%', textAlign: 'right', }}>Sub-Total</Text>
              <Text style={{ width: '30%', textAlign: 'right' }}>RM 75.50</Text>
            </View>
            <View style={{ flexDirection: 'row', justifyContent: 'flex-end', marginTop:5 }}>
              <Text style={{ width: '70%', textAlign: 'right', }}>Delivery Fee</Text>
              <Text style={{ width: '30%', textAlign: 'right' }}>RM 5.00</Text>
            </View>
            <View style={{ flexDirection: 'row', justifyContent: 'flex-end', marginTop: 15 }}>
              <Text style={{ width: '70%', textAlign: 'right', fontWeight: 'bold', }}>Total</Text>
              <Text style={{ width: '30%', textAlign: 'right', fontWeight: 'bold', color: 'green' }}>RM 80.50</Text>
            </View>
            <Text note style={{ marginTop: 10, textAlign: 'right' }}>SST Included, where applicable</Text>
          </View>

          <View style={{ padding: 20, paddingBottom: 30 }}>
            <Button full dark onPress={() => navigateActions('checkout')}>
              <Text>Proceed to Checkout</Text>
            </Button>
          </View>

        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: { backgroundColor: '#fff', flex: 1 },
})

export default AddToCart;
