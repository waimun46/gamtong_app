import React, { Component } from 'react';
import { View, Dimensions, StyleSheet, FlatList, ScrollView, Image, TouchableOpacity, Linking } from 'react-native';
import { WebView } from 'react-native-webview';
import { List, ListItem, Thumbnail, Text, Left, Body, Right, Button, Card } from 'native-base';
import ANT from 'react-native-vector-icons/AntDesign';
import MAT from 'react-native-vector-icons/MaterialIcons';
import ENT from 'react-native-vector-icons/Entypo';
import MCTI from 'react-native-vector-icons/MaterialCommunityIcons';
import img01 from '../../assest/branch/01.jpg';
import img02 from '../../assest/branch/02.jpg';
import img03 from '../../assest/branch/03.jpg';
import img04 from '../../assest/branch/04.jpg';
import img05 from '../../assest/branch/05.jpg';






const data = [
  {
    name: 'Imago', address: 'KK Times Square, Phase 2, Off Coastal Highway, 88100 Kota Kinabalu, Sabah, Malaysia',
    locate: 'Sunway Pyramid', img: require('../../assest/branch/03.jpg'), phone: '+6088274446',
    lat: '5.970556', long: '116.066389', mail: 'gamtong@hkrecipe.com'
  },
  {
    name: 'Jalan Pantai', address: 'Kinabalu Daya Hotel, Kota Kinabalu, 88000 Kota Kinabalu, Sabah, Malaysia',
    locate: 'Pavilion Kuala Lumpur', img: require('../../assest/branch/05.jpg'), phone: '+60178112828',
    lat: '5.9850434', long: '116.0765193', mail: 'contact@gamtong.com'
  },
  {
    name: 'City Mall', address: 'Block A, Ground Floor, Lot 2, DBKK NO s-0-2 City Mall, Jalan Lintas, 88300 Kota Kinabalu, Sabah',
    locate: 'Petaling Jaya, Selangor', img: require('../../assest/branch/04.jpg'), phone: '+6088486644',
    lat: '5.9616624', long: '116.094564', mail: 'contact@gamtong.com'
  },
  {
    name: '1 Borneo', address: 'Lot G-823, 1 Borneo Hypermall, Jalan Sulaman, 88400 Kota Kinabalu, Sabah, Malaysia',
    locate: 'Petaling Jaya, Selangor', img: require('../../assest/branch/01.jpg'), phone: '+6088485454',
    lat: '6.0356461', long: '116.1273715', mail: 'contact@gamtong.com'
  },
  {
    name: 'Oceanus Waterfront Mall', address: 'Jalan Tun Fuad Stephen, 88000 Kota Kinabalu, Sabah, Malaysia',
    locate: 'Petaling Jaya, Selangor', img: require('../../assest/branch/02.jpg'), phone: '+60166832525',
    lat: '5.9770282', long: '116.0667365', mail: 'Oceanus@gamtong.com'
  },
]


class LocationPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  callPhone = (item) => {
    console.log('item.phone', item.phone)
    // let phoneNumber = '';
    // if (Platform.OS === 'android') {
    //     phoneNumber = `tel:${this.props.data.tel}`;
    // }
    // else {
    //     phoneNumber = `telprompt:${this.props.data.tel}`;
    // }
    // Linking.openURL(phoneNumber);
    let phoneNumber = item.phone;
    if (Platform.OS !== 'android') {
      phoneNumber = `telprompt:${phoneNumber}`;
    }
    else {
      phoneNumber = `tel:${phoneNumber}`;
    }
    Linking.openURL(phoneNumber)
      // .then(supported => {
      // if (!supported) {
      //     console.log('Phone number is not available');
      //   } else {
      //     return Linking.openURL(phoneNumber);
      // }
      // })
      .catch(err => console.log(err));
    console.log(phoneNumber, '----phoneNumber')
  };

  _renderListLocation = ({ item, index }) => {

    return (
      <Card style={{ marginBottom: 20 }}>
        <View>
          <Image source={item.img} style={{ width: '100%', height: 200 }} />
        </View>
        <View style={{ padding: 10, marginBottom: 10, marginTop: 5 }}>
          <Text style={{ marginBottom: 10, fontWeight: 'bold' }}>{item.name}</Text>
          <View style={{ flexDirection: 'row', }}>
            <ENT name="location-pin" color={"red"} size={18} style={{ marginRight: 2 }} />
            <Text note>{item.address}</Text>
          </View>

        </View>
        <View style={{ flexDirection: 'row', justifyContent: 'space-between', backgroundColor: '#f5f5f5a6' }}>
          <TouchableOpacity style={{ width: '25%', alignItems: 'center', padding: 7 }}
            onPress={this.callPhone.bind(this, item)}>
            <MAT name="call" color={"#000"} size={25} />
          </TouchableOpacity>
          <TouchableOpacity style={{
            width: '25%', alignItems: 'center', padding: 7, borderLeftWidth: .5,
            borderRightWidth: 1, borderColor: '#77777726',
          }} onPress={() => Linking.openURL(`mailto:${item.mail}`)}
          >
            <MAT name="mail" color={"#000"} size={23} />
          </TouchableOpacity>
          <TouchableOpacity style={{
            width: '25%', alignItems: 'center', padding: 7, borderColor: '#77777726',
            borderRightWidth: 1,
          }} onPress={() => Linking.openURL(`https://waze.com/ul?ll=${item.lat},${item.long}&navigate=yes`)}
          >
            <MCTI name="waze" color={"#000"} size={25} />
          </TouchableOpacity>
          <TouchableOpacity style={{ width: '25%', alignItems: 'center', padding: 7 }}
          >
            <MAT name="restaurant-menu" color={"#000"} size={25} />
          </TouchableOpacity>
        </View>
      </Card>
      // <List>
      //   <ListItem style={{ marginLeft: 0, paddingLeft: 5 }}>
      //     <Body>
      //       <Text style={{ marginBottom: 5, fontWeight: 'bold', }}>{item.name}</Text>
      //       <Text style={{ marginBottom: 5, color: 'gray' }}>{item.address}</Text>
      //       {/* <View style={{ flexDirection: 'row' }}>
      //         <Text note>Located in :</Text>
      //         <Text note>{item.locate}</Text>
      //       </View> */}

      //     </Body>
      //     {/* <Right>
      //       <Button transparent>
      //         <Text>View</Text>
      //       </Button>
      //     </Right> */}
      //   </ListItem>
      // </List>
    );
  }

  render() {
    return (
      <View style={styles.container}>
        <ScrollView>

          {/* <WebView
            automaticallyAdjustContentInsets='false'
            // source={{ uri: 'https://g.page/HaidilaoSunway?share' }}
            source={{
              html: `
              <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d127494.0347308286!2d101.5658794896668!3d3.044080453861313!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x31cc4c871233c2e5%3A0x2152adf74a3cb071!2z5rW35bqV5o2eIEhhaWRpbGFvIEhvdCBQb3QgQFN1bndheSBQeXJhbWlk!5e0!3m2!1sen!2smy!4v1586839904347!5m2!1sen!2smy"
                width="100%" height="600" frameborder="0"
                style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"
              </iframe>
            `,
            }}
            style={{ height: 255, width: '100%', marginBottom: 10 }}
            // decelerationRate='normal'
            scalesPageToFit={true}
            javaScriptEnabled
            bounces={false}
          // domStorageEnabled
          // scrollEnabled
          /> */}



          <FlatList
            data={data}
            renderItem={this._renderListLocation}
            keyExtractor={(item, index) => index.toString()}
            contentContainerStyle={{ paddingBottom: 30, padding: 10 }}
          />

        </ScrollView >
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: { backgroundColor: '#fff', flex: 1 },
  containerMap: {
    ...StyleSheet.absoluteFillObject,
    justifyContent: 'flex-end',
    alignItems: 'center',
    height: 400,
    width: '100%'
  },

})

export default LocationPage;
