import React, { Component } from 'react';
import { View, StyleSheet, TextInput, Dimensions, Image, ScrollView, FlatList, TouchableOpacity } from 'react-native';
import { Button, Text, Card, CardItem, Item, Input } from 'native-base';
import ANT from 'react-native-vector-icons/AntDesign';
import FET from 'react-native-vector-icons/Feather';
import MTC from 'react-native-vector-icons/MaterialCommunityIcons';
import Carousel from 'react-native-snap-carousel';

const numColumns = 2;

const itemWidth = Dimensions.get('window').width;
const itemHeight = 220;

const { width: viewportWidth } = Dimensions.get('window');

const dataFlyer = [
  { img: require('../../assest/dimsum/09.png') },
  { img: require('../../assest/dimsum/10.png') },
  { img: require('../../assest/dimsum/11.png') },
]

const data = [
  {
    item: 'PRODUCT 1', name: 'This is my products.', price: '40.00', discount: '15%',
    img: require('../../assest/dimsum/01.png'), star: 4.6
  },
  {
    item: 'PRODUCT 2', name: 'This is my products.', price: '35.50', discount: '',
    img: require('../../assest/dimsum/02.png'), star: 3.6
  },
  {
    item: 'PRODUCT 3', name: 'This is my products.', price: '20.60', discount: '25%',
    img: require('../../assest/dimsum/03.png'), star: 4.5
  },
  {
    item: 'PRODUCT 4', name: 'This is my products.', price: '46.40', discount: '30%',
    img: require('../../assest/dimsum/04.png'), star: 3.5
  },
  
]

const dataSeller = [
  {
    item: 'PRODUCT 1', name: 'This is my products.', price: '40.00', discount: '15%',
    img: require('../../assest/dimsum/05.png'), star: 4.6
  },
  {
    item: 'PRODUCT 2', name: 'This is my products.', price: '35.50', discount: '',
    img: require('../../assest/dimsum/06.png'), star: 3.6
  },
  {
    item: 'PRODUCT 3', name: 'This is my products.', price: '20.60', discount: '25%',
    img: require('../../assest/dimsum/07.png'), star: 4.5
  },
  {
    item: 'PRODUCT 4', name: 'This is my products.', price: '46.40', discount: '30%',
    img: require('../../assest/dimsum/08.png'), star: 3.5
  },
  {
    item: 'PRODUCT 5', name: 'This is my products.', price: '40.00', discount: '15%',
    img: require('../../assest/dimsum/01.png'), star: 4.6
  },
  {
    item: 'PRODUCT 6', name: 'This is my products.', price: '35.50', discount: '',
    img: require('../../assest/dimsum/02.png'), star: 3.6
  },
  {
    item: 'PRODUCT 7', name: 'This is my products.', price: '20.60', discount: '25%',
    img: require('../../assest/dimsum/03.png'), star: 4.5
  },
  {
    item: 'PRODUCT 8', name: 'This is my products.', price: '46.40', discount: '30%',
    img: require('../../assest/dimsum/04.png'), star: 3.5
  },
]

class HomePage extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  /**************************************** renderSlider ************************************/
  _renderSlider = ({ item, index }) => {
    const navigateActions = this.props.navigation.navigate;
    return (
      <View style={styles.slide}>
        <TouchableOpacity onPress={() => navigateActions('productDetails', { data: item })}
          style={styles.carouselContainer}>
          <Image source={require('../../assest/dimsun.jpg')} style={styles.imageSty} />
        </TouchableOpacity>
      </View>
    );
  }

  /**************************************** renderBestSeller ************************************/
  _renderBestSeller = ({ item }) => {
    const navigateActions = this.props.navigation.navigate;
    return (
      <View style={styles.itemWarp}>
        <TouchableOpacity onPress={() => navigateActions('productDetails', { data: item })} >
          <Card style={{ position: 'relative' }}>
            <CardItem cardBody >
              <Image source={item.img} style={styles.itemProductImg} />
            </CardItem>
            <TouchableOpacity style={styles.addWish}>
            <Card style={styles.addWish}>
                <ANT name="heart" color={"#FF6347"} size={15} />
            </Card>
            </TouchableOpacity>
            <View style={[styles.tagSty, { backgroundColor: item.discount === '' ? 'transperent' : '#000' }]}>
              <View style={{ alignItems: 'center', }}>
                {
                  item.discount === '' ?
                    <View style={[styles.itemAddBtn, { borderColor: 'transperent', borderWidth: 0 }]}>
                      <Text note style={[styles.itemAddText, { color: 'transperent' }]}>{item.discount} </Text>
                    </View>
                    :
                    <View style={styles.itemAddBtn}>
                      <Text note style={styles.itemAddText}>{item.discount} OFF</Text>
                    </View>
                }
              </View>
            </View>
            <CardItem style={{ paddingLeft: 5, paddingRight: 5, }}>
              <View style={{ width: '100%' }}>
                <Text numberOfLines={1} style={{ fontWeight: '600', }}>{item.item}</Text>
                <Text numberOfLines={1} style={styles.itemSubtitle}>{item.name}</Text>
                {/* <View style={{ flexDirection: 'row', marginBottom: 5 }}>
                  <ANT name="star" color="#FFD700" size={15} style={{ paddingRight: 5 }} />
                  <Text note style={{ color: '#000' }}>{item.star}</Text>
                </View> */}
                <Text style={{ fontWeight: 'bold' }}>RM {item.price}</Text>
              </View>
            </CardItem>
          </Card>
        </TouchableOpacity>
      </View>
    )
  }

  /**************************************** renderFlyer ************************************/
  _renderFlyer = ({ item }) => {
    const navigateActions = this.props.navigation.navigate;
    return (
      <View style={[styles.itemWarp, { width: '100%', marginBottom: 20 }]}>
        <TouchableOpacity onPress={() => navigateActions('productDetails', { data: item })} >
          <Card style={{ position: 'relative' }}>
            <CardItem cardBody >
              <View style={{width: '100%', height: 200}}>
                <Image source={item.img} style={{width: '100%', height: '100%', resizeMode: 'contain'}} />
              </View>

            </CardItem>
          </Card>
        </TouchableOpacity>
      </View>
    )
  }




  render() {
    const navigateActions = this.props.navigation.navigate;
    console.log('screenprops', this.props)

    return (
      <View style={styles.container}>

        {/************************************* SEARCH ****************************************/}
        <View style={styles.searchWrap}>
          <Card style={styles.searchCardWarp}>
            <CardItem style={styles.searchCardItem}>
              <Item style={{ borderBottomWidth: 0 }}>
                <TouchableOpacity>
                  <ANT name="search1" color="#f48120" size={18} style={{ paddingRight: 10 }} />
                </TouchableOpacity>
                <Input autoCorrect={false} autoCapitalize="none" placeholder="Search" placeholderTextColor="#ccc" />
                <TouchableOpacity onPress={() => navigateActions('brands')}>
                  <FET name="sliders" color="#f48120" size={18} />
                </TouchableOpacity>
              </Item>
            </CardItem>
          </Card>
        </View>

        {/************************************* TOP BUTTOM ****************************************/}
        {/* <View style={styles.topBtnWarp}>
          <TouchableOpacity onPress={() => navigateActions('catagory')} iconLeft full style={[styles.topBtn, { borderRightWidth: .5, borderRightColor: '#fff' }]}>
            <ANT name="appstore-o" color="#fff" size={18} />
            <Text style={styles.topBtnTextColor} note>Category</Text>
          </TouchableOpacity>
          <TouchableOpacity onPress={() => navigateActions('brands')} iconLeft full style={styles.topBtn}>
            <ANT name="tago" color="#fff" size={18} />
            <Text style={styles.topBtnTextColor} note>Brands</Text>
          </TouchableOpacity>
        </View> */}

        <ScrollView>

          {/************************************* CAROUSEL SLIDER ****************************************/}
          <Carousel
            data={data}
            renderItem={this._renderSlider}
            sliderWidth={viewportWidth}
            itemWidth={viewportWidth}
            autoplay={true}
            autoplayDelay={3000}
            autoplayInterval={3000}
            loop={true}
            enableMomentum={true}
          />

          {/************************************* OUR BEST SELLERS ****************************************/}
          <View>
            <View style={styles.productTitle}>
              <Text style={{ fontWeight: '600' }}>Our Best Sellers</Text>
            </View>
            <FlatList
              data={dataSeller}
              // numColumns={numColumns}
              horizontal={true}
              renderItem={this._renderBestSeller}
              keyExtractor={(item, index) => index.toString()}
            />
          </View>

          {/************************************* Flyer ****************************************/}
          <View>
            <View style={styles.productTitle}>
              <Text style={{ fontWeight: '600' }}>Fragrance sales up to 50% OFF</Text>
            </View>
            <FlatList
              data={dataFlyer}
              renderItem={this._renderFlyer}
              keyExtractor={(item, index) => index.toString()}
              contentContainerStyle={{ paddingBottom: 50 }}
            />
          </View>


        </ScrollView>






      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: { backgroundColor: '#fff', flex: 1 },
  searchWrap: { paddingLeft: 10, paddingRight: 10, paddingBottom: 5, paddingTop: 5, backgroundColor: '#f48120', },
  searchCardWarp: { borderRadius: 8, },
  searchCardItem: { paddingTop: 0, paddingBottom: 0, borderRadius: 8, },
  topBtnWarp: { flexDirection: 'row', marginTop: 0, marginLeft: 0, marginRight: 0, marginBottom: 0, borderTopWidth: 0, },
  topBtn: { backgroundColor: '#FF6347', width: '50%' },
  topBtnTextColor: { color: '#fff', padding: 15, textAlign: 'center' },
  slide: { width: itemWidth, height: itemHeight, },
  carouselContainer: {
    shadowColor: '#000', shadowOffset: { width: 0, height: 1 },
    shadowOpacity: 0.8, shadowRadius: 2, elevation: 20, position: 'relative'
  },
  imageSty: { width: '100%', height: '100%', resizeMode: 'cover' },
  addWish: {
    position: 'absolute', top: '50%', right: 0, padding: 8, borderRadius: 100, zIndex: 1, elevation: 1, 
    // justifyContent : 'center', alignItems: 'center', flex: 1, 
  },
  tagSty: {
    position: 'absolute', top: '0%', left: 0, zIndex: 1, elevation: 1,
    // justifyContent : 'center', alignItems: 'center', flex: 1, 
  },
  itemWarp: { width: 200, padding: 5, paddingTop: 0 },
  itemSubtitle: { color: 'gray', marginBottom: 5, marginTop: 3 },
  itemAddWarp: { paddingLeft: 0, paddingRight: 0, paddingTop: 0, paddingBottom: 0 },
  itemAddBtn: { padding: 5 },
  itemAddText: { width: '100%', textAlign: 'center', color: '#fff', },
  itemProductImg: { height: 120, width: null, flex: 1 },
  productTitle: { marginTop: 15, paddingBottom: 10, paddingLeft: 10, paddingRight: 10 },

});


export default HomePage;
