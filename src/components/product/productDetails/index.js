import React, { Component } from 'react';
import { View, StyleSheet, Image, TouchableOpacity, ScrollView } from 'react-native';
import { Button, Text, Card, CardItem, Item, Input } from 'native-base';
import ANT from 'react-native-vector-icons/AntDesign';
import MAT from 'react-native-vector-icons/MaterialIcons';
import NumericInput from 'react-native-numeric-input'


class ProductDetails extends Component {
  constructor(props) {
    super(props);
    this.state = {
      valueAdd: 1
    };
  }

  render() {
    console.log('ProductDetails', this.props.navigation.state.params.data);
    const dataProps = this.props.navigation.state.params.data;
    const navigateActions = this.props.navigation.navigate;


    return (
      <View style={styles.container}>
        <ScrollView>
          <View style={{ position: 'relative' }}>
            <Image source={dataProps.img} style={styles.imgSty} />
            <View style={styles.overlayBtn}>
              <TouchableOpacity onPress={() => this.props.navigation.goBack(null)}>
                <Card style={{ borderRadius: 30, }}>
                  <View style={styles.backBtn}>
                    <ANT name="left" color={"#fff"} size={20} style={{ padding: 8, }} />
                  </View>
                </Card>
              </TouchableOpacity>
              <TouchableOpacity onPress={() => navigateActions('addToCart')}>
                <Card style={{ borderRadius: 30, }}>
                  <View style={styles.cartBtn}>
                    <MAT name="shopping-cart" color={"#fff"} size={22} style={{ padding: 8, }} />
                  </View>
                </Card>
              </TouchableOpacity>
            </View>
          </View>

          <View style={styles.detailWarp}>
            <View style={{ flexDirection: 'row', justifyContent: 'space-between', }}>
              <View >
                <Text style={{ color: 'green' }}>Avalible</Text>
              </View>
              <TouchableOpacity style={styles.addWish}>
                <Card style={styles.addWish}>
                  <ANT name="heart" color={"#FF6347"} size={15} />
                </Card>
              </TouchableOpacity>
              {/* <View style={{ flexDirection: 'row', marginBottom: 5 }}>
                <ANT name="star" color="#FFD700" size={15} style={{ paddingRight: 5 }} />
                <Text note style={{ color: '#000' }}>{dataProps.star}</Text>
              </View> */}
            </View>
            <View>
              <Text style={{ marginTop: 10, marginBottom: 5, fontWeight: 'bold' }}>{dataProps.item}</Text>
              <Text note>{dataProps.name}</Text>
              <View style={styles.priceWarp}>
                <Text style={{ marginTop: 10, fontWeight: 'bold' }}>RM {dataProps.price}</Text>
                {/* <View style={styles.tagWarp}>
                  <Text note style={{ color: '#fff', }}>3 FOR RM 110</Text>
                </View> */}
              </View>
            </View>

            <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginTop: 20 }}>
              <View style={{ paddingRight: 20, width: '60%' }}>
                <NumericInput
                  value={this.state.valueAdd}
                  onChange={(valueAdd) => { this.setState({ valueAdd }); console.log(valueAdd) }}
                  onLimitReached={(isMax, msg) => console.log(isMax, msg)}
                  totalWidth={160}
                  totalHeight={30}
                  iconSize={25}
                  step={1}
                  valueType='real'
                  rounded
                  textColor='#000'
                  borderColor='#ccc'
                  iconStyle={{ color: 'white' }}
                  rightButtonBackgroundColor='#000'
                  leftButtonBackgroundColor='#000'
                  minValue={1}
                // maxValue={9999}
                />
              </View>
              <TouchableOpacity style={{
                borderWidth: .5, borderColor: '#000',
                justifyContent: 'center', paddingLeft: 15, paddingRight: 15
              }}
                onPress={() => navigateActions('addToCart')}
              >
                <Text note style={{ color: '#000' }}>ADD TO CART</Text>
              </TouchableOpacity>
            </View>
          </View>

          <View style={styles.detailWarp}>
            <Text style={{ fontWeight: 'bold', marginBottom: 5 }}>About This Product</Text>
            <Text note>
              Lorem Ipsum is simply dummy text of the printing and typesetting industry.
              Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,
              when an unknown printer took a galley of type and scrambled it to make a
              type specimen book.
          </Text>

            {/* <View style={{ marginTop: 20 }}>
              <Text>Reviews(s)</Text>
              <TouchableOpacity style={{
                borderWidth: .5, borderColor: '#000', width: '50%', marginTop: 10,
                justifyContent: 'center', paddingLeft: 15, paddingRight: 15, backgroundColor: '#000'
              }}>
                <Text note style={{ color: '#fff', padding: 12, textAlign: 'center' }}>Write a review!</Text>
              </TouchableOpacity>
            </View> */}

          </View>


        </ScrollView>

      </View>
    );
  }
}


const styles = StyleSheet.create({
  container: { flex: 1, backgroundColor: '#fff' },
  itemProductImg: { height: 120, width: null, flex: 1 },
  overlayBtn: {
    flexDirection: "row", justifyContent: 'space-between', position: 'absolute', top: '12%',
    width: '100%', paddingLeft: 10, paddingRight: 10,
  },
  tagWarp: {
    marginTop: 10, marginBottom: 5, backgroundColor: '#f48120', paddingTop: 5, paddingBottom: 5, paddingLeft: 10,
    paddingRight: 10, borderRadius: 20
  },
  backBtn: { borderRadius: 30, backgroundColor: '#f48120', paddingBottom: 0, paddingTop: 0, height: 'auto' },
  cartBtn: { borderRadius: 30, backgroundColor: '#f48120', paddingBottom: 0, paddingTop: 0, height: 'auto' },
  detailWarp: { backgroundColor: '#fff', padding: 20 },
  imgSty: { width: '100%', height: 250 },
  priceWarp: { flexDirection: 'row', justifyContent: 'space-between' },
  addWish: {
    position: 'absolute', top: '50%', right: 0, padding: 8, borderRadius: 100, zIndex: 1, elevation: 1,
    // justifyContent : 'center', alignItems: 'center', flex: 1, 
  },

});

export default ProductDetails;
