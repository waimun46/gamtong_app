import React, { Component } from 'react';
import { View, StyleSheet, TextInput, Dimensions, Image, ScrollView, FlatList, TouchableOpacity } from 'react-native';
import { Button, Text, Card, CardItem, Item, Input } from 'native-base';
import ANT from 'react-native-vector-icons/AntDesign';

const numColumns = 2;

const data = [
  { item: 'PRODUCT 1', name: 'This is my products.', price: '40.00', img: require('../../assest/dimsum/11.png'), star: 4.6 },
  { item: 'PRODUCT 2', name: 'This is my products.', price: '35.50', img: require('../../assest/dimsum/12.png'), star: 3.6 },
  { item: 'PRODUCT 3', name: 'This is my products.', price: '20.60', img: require('../../assest/dimsum/13.png'), star: 4.5 },
  { item: 'PRODUCT 4', name: 'This is my products.', price: '46.40', img: require('../../assest/dimsum/14.png'), star: 3.5 },
  { item: 'PRODUCT 5', name: 'This is my products.', price: '46.40', img: require('../../assest/dimsum/15.png'), star: 3.5 },
  { item: 'PRODUCT 6', name: 'This is my products.', price: '46.40', img: require('../../assest/dimsum/16.png'), star: 3.5 },
  { item: 'PRODUCT 7', name: 'This is my products.', price: '46.40', img: require('../../assest/dimsum/17.png'), star: 3.5 },
  { item: 'PRODUCT 8', name: 'This is my products.', price: '46.40', img: require('../../assest/dimsum/18.png'), star: 3.5 },
]

class ProductPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  /**************************************** _renderProduct ************************************/
  _renderProduct = ({ item }) => {
    const navigateActions = this.props.navigation.navigate;
    return (
      <View style={styles.itemWarp}>
        <TouchableOpacity onPress={() => navigateActions('productDetails', { data: item })}>
          <Card style={{ position: 'relative' }}>
            <CardItem cardBody >
              <Image source={item.img} style={styles.itemProductImg} />
            </CardItem>
            <TouchableOpacity style={styles.addWish}>
              <Card style={styles.addWish}>
                <ANT name="heart" color={"#FF6347"} size={15} />
              </Card>
            </TouchableOpacity>
            <CardItem style={{ paddingLeft: 5, paddingRight: 5 }}>
              <View>
                <Text note numberOfLines={1} style={{ fontWeight: '600', color: '#000' }}>{item.item}</Text>
                <Text note numberOfLines={1} style={styles.itemSubtitle}>{item.name}</Text>
                {/* <View style={{ flexDirection: 'row', marginBottom: 5 }}>
                  <ANT name="star" color="#FFD700" size={15} style={{ paddingRight: 5 }} />
                  <Text note style={{ color: '#000' }}>{item.star}</Text>
                </View> */}
                <Text style={{ fontWeight: 'bold' }}>RM {item.price}</Text>
              </View>
            </CardItem>
            {/* <CardItem footer bordered style={styles.itemAddWarp}>
              <TouchableOpacity style={styles.itemAddBtn}>
                <Text note style={styles.itemAddText}>ADD TO CART</Text>
              </TouchableOpacity>
            </CardItem> */}
          </Card>
        </TouchableOpacity>
      </View>
    )
  }

  render() {
    const navigateActions = this.props.navigation.navigate;
    console.log('ProductPage', this.props)
    return (
      <View style={styles.container}>
        {/************************************* PRODUCT ****************************************/}
        <View>
          <View style={styles.productTitle}>
            <Text note >100 items found</Text>
          </View>
          <FlatList
            data={data}
            numColumns={numColumns}
            renderItem={this._renderProduct}
            keyExtractor={(item, index) => index.toString()}
            contentContainerStyle={{ paddingBottom: 50 }}
          />
        </View>

      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: { backgroundColor: '#fff', flex: 1 },
  addWish: {
    position: 'absolute', top: '43%', right: 0, padding: 8, borderRadius: 100, zIndex: 1, elevation: 1,
    // justifyContent : 'center', alignItems: 'center', flex: 1
  },
  itemWarp: { width: '50%', padding: 5, paddingTop: 0 },
  itemSubtitle: { color: 'gray', marginBottom: 5, marginTop: 3 },
  itemAddWarp: { paddingLeft: 0, paddingRight: 0, paddingTop: 0, paddingBottom: 0, backgroundColor: '#000' },
  itemAddBtn: { width: '100%', padding: 10, borderColor: 'gray', borderWidth: .5 },
  itemAddText: { textAlign: 'center', color: '#fff', fontWeight: '400', },
  itemProductImg: { height: 120, width: null, flex: 1 },
  productTitle: { marginTop: 15, paddingBottom: 10, paddingLeft: 10, paddingRight: 10 },

});

export default ProductPage;
