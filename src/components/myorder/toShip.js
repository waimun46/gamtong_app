import React, { Component } from 'react';
import { View, StyleSheet, ScrollView, TouchableOpacity, FlatList } from 'react-native';
import { Text, ListItem, Left, Body, Right, Button, Thumbnail, List, Item } from 'native-base';
import ANT from 'react-native-vector-icons/AntDesign';
import FET from 'react-native-vector-icons/Feather';


const data = [
  {
    item: 'PRODUCT 1', name: 'This is my products.', price: '40.00', date: '04-04-2020', status: 'Pick up',
    img: require('../../assest/dimsum/01.png'),
  },
  {
    item: 'PRODUCT 2', name: 'This is my products.', price: '20.00', date: '04-04-2020', status: 'Proseccing',
    img: require('../../assest/dimsum/02.png'),
  },
  {
    item: 'PRODUCT 3', name: 'This is my products.', price: '10.00', date: '04-04-2020', status: 'Pick up',
    img: require('../../assest/dimsum/04.png'),
  },
]

class ToShipScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  _renderShip = ({ item, index }) => {
    const navigateActions = this.props.navigation.navigate;
    return (
        <List style={{ backgroundColor: '#fff', marginTop: 10, }} >
          <ListItem avatar style={{ paddingBottom: 15, marginLeft: 10 }}>
            <Left style={{ borderBottomWidth: 0 }}>
              <Thumbnail square source={item.img} style={{ width: 80, height: 80 }} />
            </Left>
            <Body style={{ borderBottomWidth: 0 }}>
              <Text>{item.item}</Text>
              <Text note style={{ marginTop: 5 }}>{item.name}</Text>
              {/* <View style={{ flexDirection: 'row', marginTop: 5 }}>
              <ANT name="star" color="#FFD700" size={15} style={{ paddingRight: 5 }} />
              <Text note style={{ color: '#000' }}>{item.star}</Text>
            </View> */}
              <View style={{ flexDirection: 'row', marginTop: 10, justifyContent: 'space-between' }}>
                <Text style={{ fontWeight: 'bold' }}>RM {item.price}</Text>

              </View>

            </Body>
          </ListItem>
          <ListItem style={{ borderTopWidth: .5, paddingLeft: 0, marginLeft: 0, borderTopColor: '#eee' }}>
            <Body style={{ borderBottomWidth: 0 }}>
              <View style={{ flexDirection: 'row', paddingLeft: 10, justifyContent: 'space-between' }}>
                <View style={{ flexDirection: 'row', }}>
                  <FET name="truck" size={18} style={{ color: '#f48120', }} />
                  <Text note>[{item.date}] Item {item.status}</Text>
                </View>
                <View style={[styles.statusColor, { backgroundColor: item.status === 'Proseccing' ? 'red' : '#ffb441', }]} />
              </View>
            </Body>
          </ListItem>
        </List>
    )
  }

  render() {
    return (
      <View style={styles.container}>
        <ScrollView>

          <FlatList
            data={data}
            renderItem={this._renderShip}
            keyExtractor={(item, index) => index.toString()}
            contentContainerStyle={{ paddingBottom: 50 }}
          />

        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: { flex: 1, },
  title: { marginTop: 15, paddingBottom: 10, paddingLeft: 10, paddingRight: 10 },
  statusColor: { borderRadius: 50, width: 12, height: 12, marginTop: 3 }


})

export default ToShipScreen;
