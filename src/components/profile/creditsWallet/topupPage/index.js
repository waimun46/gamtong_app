import React, { Component } from 'react';
import { View, ScrollView, StyleSheet, TextInput } from 'react-native';
import { Text, Button } from 'native-base';
import { showMessage, hideMessage } from "react-native-flash-message";
import { RFPercentage, RFValue } from "react-native-responsive-fontsize";


class EwalletTopUp extends Component {
  constructor(props) {
    super(props);
    this.state = {
      amount: '',
      mmidTokenForget: '',
      restartTime: Date.now() + 180000,
      isSubmit: false

    };
  }

  /****************************************** onChangeTextInput ********************************************/
  onChangeTextInput(text, field) {
    //console.log(text)
    if (field === 'amount') { this.setState({ amount: text }) }
  }



  render() {
    const { amount, mmidTokenForget, restartTime, isSubmit } = this.state;
    const navigateActions = this.props.navigation.navigate;

    return (
      <ScrollView>

        <View style={{ flex: 1, alignItems: 'center', backgroundColor: ' #fff' }}>
          {/************************** code input ****************************/}
          <View style={{ width: '80%', marginTop: 30 }}>
            <Text note style={{ color: 'blue', }}>Enter your preferred amount</Text>
            <View style={{
              flexDirection: 'row', alignItems: 'center', backgroundColor: '#eee',
              borderBottomWidth: .5, borderBottomColor: 'blue', marginBottom: 20, marginTop: 5
            }}>
              <View style={{ width: '13%', }}>
                <Text style={{ fontSize: RFPercentage(3), color: 'blue', fontWeight: 'bold', }}>RM</Text>
              </View>

              <View style={{ width: '87%', }}>
                <TextInput
                  value={amount}
                  placeholder=""
                  placeholderTextColor="#ccc"
                  style={styles.inputsty}
                  onChangeText={(text) => this.onChangeTextInput(text, 'amount')}
                  autoCorrect={false}
                  autoCapitalize="none"
                // keyboardType={'numeric'}
                />
              </View>

            </View>

            {/************************** button submit ****************************/}
            <Button full dark style={styles.btnwarp} onPress={() => navigateActions('ordinary_Infor')}>
              <Text style={styles.btntext}>TopUp</Text>
            </Button>


          </View>

        </View>

      </ScrollView>
    );
  }
}


const styles = StyleSheet.create({
  inputsty: {
    height: 50, borderColor: '#ccc', color: 'blue', padding: 10, fontWeight: 'bold',
    textAlign: 'left', letterSpacing: 1, fontSize: RFPercentage(3), width: '100%'
  },
  btnwarp: { marginTop: 30, borderRadius: 5, height: 50, },
  btntext: { color: '#fff', },
  resendBtn: { height: 30, justifyContent: 'center', alignItems: 'flex-end' },
  timeCount: { height: 30, justifyContent: 'flex-end', alignItems: 'flex-end' },
  secondText: { color: 'red', fontSize: 18, textAlign: 'right' },
  modalBackground: { flex: 1, alignItems: 'center', flexDirection: 'column', justifyContent: 'space-around', backgroundColor: '#00000040' },
  activityIndicatorWrapper: {
    backgroundColor: '#FFFFFF', height: 100, width: 100, borderRadius: 10, display: 'flex',
    alignItems: 'center', justifyContent: 'space-around'
  },

});

export default EwalletTopUp;
