import React, { Component } from 'react';
import { View, StyleSheet } from 'react-native';
import { Form, Item, Input, Label, Text, Button } from 'native-base';


class ResetPass extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  render() {
    const navigateActions = this.props.navigation.navigate;

    return (
      <View style={styles.container}>
        <Form>
          <Item stackedLabel last>
            <Label>New Password</Label>
            <Input placeholder='Fill in your new password' placeholderTextColor='#ccc' />
          </Item>
          <Item stackedLabel last>
            <Label>Confirm New Password</Label>
            <Input placeholder='Fill in your confirm new password' placeholderTextColor='#ccc' />
          </Item>
        </Form>
        {/* <View style={{ padding: 20 }}>
          <Text note style={{ textAlign: 'left' }}>
            Please enter your phone number in field above, once completed, you will receive a phone SMS containing a confirmation code shortly.
            This confirmation code will be used for the final step in resetting your phone number.
          </Text>
        </View> */}

        <View style={{ width: '100%', alignItems: 'center', marginTop: 30, }}>
          <View style={{ width: '80%', }}>
            <Button onPress={() => navigateActions('home')} full dark >
              <Text >RESET</Text>
            </Button>
          </View>

        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: { backgroundColor: '#fff', flex: 1 },

})

export default ResetPass;
