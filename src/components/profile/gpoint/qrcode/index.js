import React, { Component } from 'react';
import { View, StyleSheet, ScrollView, TouchableOpacity, Share } from 'react-native';
import { Card, Content, Text, ListItem, Left, Body, Right, Button, Thumbnail } from 'native-base';
import QRCode from 'react-native-qrcode-svg';


class GPointQRCodeScan extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  render() {
    return (
      <ScrollView>
        <View style={styles.container}>
          <View style={{ flex: 1, alignItems: 'center', }}>
            <View style={{ width: '80%', marginTop: 30, }}>
              <Card style={{ alignItems: 'center', padding: 30, paddingTop: 40, }}>
                <QRCode
                  value="https://fishmarketrestaurantkk.com/gt/_adminCP/login"
                  size={200}
                />
                <View style={{ marginTop: 30 }}>
                  <Text style={{color:'#f48120'}}>Scan and Pay your item</Text>
                </View>
              </Card>
            </View>
          </View>

          <View style={{ marginTop: 20 }}>
            <Card style={styles.typeScan}>
              <Text style={{ color: '#f48120', fontWeight: 'bold' }}>G - Point</Text>
              <Text>300</Text>
            </Card>
          </View>

        </View>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: { flex: 1, },
  typeScan: {
    flexDirection: 'row', justifyContent: 'space-between', backgroundColor: '#fff', padding: 20,
  }
})

export default GPointQRCodeScan;
