import React, { Component } from 'react';
import { View, StyleSheet, ScrollView, TouchableOpacity, } from 'react-native';
import { Content, Text, List, ListItem, Left, Body, Right, Button, Thumbnail, Card } from 'native-base';
import { RFPercentage, RFValue } from "react-native-responsive-fontsize";
import ANT from 'react-native-vector-icons/AntDesign';
import MCI from 'react-native-vector-icons/MaterialCommunityIcons';


class GPoints extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  render() {
    const navigateActions = this.props.navigation.navigate;

    return (
      <View style={styles.container}>
        <ScrollView>
          <View style={styles.pointBg}>
            <View style={{ alignItems: 'center' }}>
              <Text style={{ color: '#fff', fontSize: RFPercentage('6'), marginBottom: 10, }}>300</Text>
              <Text note style={{ color: '#fff', }}>Your Available Points</Text>
            </View>
          </View>
          <View style={{ padding: 20 }}>
            {/********************** Show QR Code **********************/}
            <Card style={{ marginBottom: 20 }}>
              <List>
                <ListItem thumbnail onPress={() => navigateActions('gpoint_qr_scan')}>
                  <Left>
                    <MCI name="qrcode-scan" size={25} style={{ color: '#f48120' }} />
                  </Left>
                  <Body>
                    <Text>Show QR Code</Text>
                    <Text note numberOfLines={1}>Scan and Pay </Text>
                  </Body>
                  <Right>
                    <ANT name="right" size={18} style={{ color: '#ccc' }} />
                  </Right>
                </ListItem>
              </List>
            </Card>
            {/********************** Transfer History **********************/}
            <Card style={{ marginBottom: 20 }}>
              <List>
                <ListItem thumbnail onPress={() => navigateActions('credit_infor')}>
                  <Left>
                    <ANT name="retweet" size={25} style={{ color: '#f48120' }} />
                  </Left>
                  <Body>
                    <Text>Transfer History</Text>
                    <Text note numberOfLines={1}>View all your transfer history</Text>
                  </Body>
                  <Right>
                    <ANT name="right" size={18} style={{ color: '#ccc' }} />
                  </Right>
                </ListItem>
              </List>
            </Card>
          </View>
        </ScrollView>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: { flex: 1, },
  iconsty: { color: '#fff', },
  pointBg: {
    backgroundColor: '#f48120c7', padding: 10, height: 150, alignItems: 'center',
    justifyContent: 'center'
  },

})

export default GPoints;
