import React, { Component } from 'react';
import { View, StyleSheet, ScrollView, FlatList, TouchableOpacity } from 'react-native';
import { Card, CardItem, Text, } from 'native-base';
import { RFPercentage, RFValue } from "react-native-responsive-fontsize";


const data = [
  { discount: '10%', caption: '10% Discount form this item.', price: '20', exp: '30-04-2020' },
  { discount: '20%', caption: '20% Discount form this item.', price: '30', exp: '28-04-2020' },
  { discount: '20%', caption: '20% Discount form this item.', price: '30', exp: '20-04-2020' },
  { discount: '10%', caption: '10% Discount form this item.', price: '20', exp: '30-04-2020' },
]


class CouponPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  _renderCoupon = ({ item, index }) => {
    const navigateActions = this.props.navigation.navigate;
    return (
      <Card style={{marginBottom: 15}}>
        <TouchableOpacity onPress={() => navigateActions('barcode', {data: item})} style={{ flexDirection: 'row', }}>
          <View style={styles.discountWarp}>
            <View style={styles.discountInner}>
              <Text style={styles.discountText}>{item.discount}</Text>
              <Text note style={styles.discountMention}>Discount</Text>
            </View>
          </View>

          <View style={styles.contentWarp}>
            <Text >{item.caption}</Text>
            <Text style={styles.contentPrice}>RM {item.price}</Text>
            <Text note style={{ marginTop: 10 }}>Exp date : {item.exp}</Text>
          </View>
        </TouchableOpacity>
      </Card>
    )
  }

  render() {
    return (
      <View style={styles.container}>
        <ScrollView>

          <View style={styles.productTitle}>
            <Text note >Select Your Coupon</Text>
          </View>

          <FlatList
            data={data}
            renderItem={this._renderCoupon}
            keyExtractor={(item, index) => index.toString()}
            contentContainerStyle={{ padding: 10 }}
          />



        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: { flex: 1, },
  productTitle: { marginTop: 15, paddingBottom: 0, paddingLeft: 10, paddingRight: 10 },
  discountWarp: { width: '30%', backgroundColor: '#FF6347', padding: 10, paddingTop: 20, paddingBottom: 20 },
  discountInner: { justifyContent: 'center', alignItems: 'center', flex: 1 },
  discountText: { textAlign: 'center', color: '#fff', fontSize: RFPercentage(4.8), fontWeight: '500', },
  discountMention: { textAlign: 'center', color: '#fff', },
  contentWarp: { width: '70%', padding: 10 },
  contentPrice: { fontWeight: 'bold', marginTop: 10 },

})

export default CouponPage;
