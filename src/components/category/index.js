import React, { Component } from 'react';
import { View, FlatList, StyleSheet, Image, TouchableOpacity, ScrollView } from 'react-native';
import { Button, Text, Card, CardItem, Item, Input } from 'native-base';
import ANT from 'react-native-vector-icons/AntDesign';
import MTC from 'react-native-vector-icons/MaterialCommunityIcons';



const data = [
  { image: 'https://cdn.nohat.cc/thumb/f/720/0fec843cd5c84a2d9514.jpg', item: 'PRODUCT 1' },
  { image: 'https://cdn.nohat.cc/thumb/f/720/0fec843cd5c84a2d9514.jpg', item: 'PRODUCT 2' },
  { image: 'https://cdn.nohat.cc/thumb/f/720/0fec843cd5c84a2d9514.jpg', item: 'PRODUCT 3' },
  { image: 'https://cdn.nohat.cc/thumb/f/720/0fec843cd5c84a2d9514.jpg', item: 'PRODUCT 4' },
  { image: 'https://cdn.nohat.cc/thumb/f/720/0fec843cd5c84a2d9514.jpg', item: 'PRODUCT 5' },
]

const dataSeller = [
  {
    item: 'PRODUCT 1', name: 'This is my products.', price: '40.00', discount: '15%',
    img: 'https://cdn.nohat.cc/thumb/f/720/0fec843cd5c84a2d9514.jpg', star: 4.6
  },
  {
    item: 'PRODUCT 2', name: 'This is my products.', price: '35.50', discount: '',
    img: 'https://cdn.nohat.cc/thumb/f/720/0fec843cd5c84a2d9514.jpg', star: 3.6
  },
  {
    item: 'PRODUCT 3', name: 'This is my products.', price: '20.60', discount: '25%',
    img: 'https://cdn.nohat.cc/thumb/f/720/0fec843cd5c84a2d9514.jpg', star: 4.5
  },
  {
    item: 'PRODUCT 4', name: 'This is my products.', price: '46.40', discount: '30%',
    img: 'https://cdn.nohat.cc/thumb/f/720/0fec843cd5c84a2d9514.jpg', star: 3.5
  },
]


class CatagoryPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  _renderTop = ({ item }) => {
    const navigateActions = this.props.navigation.navigate;
    return (
      <View style={styles.itemWarp}>
        <TouchableOpacity onPress={() => navigateActions('product')}>
          <View style={{ position: 'relative' }}>
            <CardItem cardBody >
              <Image source={{ uri: item.image }} style={styles.itemProductImg} />
            </CardItem>
            <CardItem style={styles.itemTextWarp}>
              <Text note numberOfLines={1} style={styles.itemText}>{item.item}</Text>
            </CardItem>
          </View>
        </TouchableOpacity>
      </View>
    )
  }


  /**************************************** renderBestSeller ************************************/
  _renderNew = ({ item }) => {
    const navigateActions = this.props.navigation.navigate;
    return (
      <View style={styles.itemWarp}>
        <TouchableOpacity onPress={() => navigateActions('productDetails', {data: item})} >
          <Card style={{ position: 'relative' }}>
            <CardItem cardBody >
              <Image source={{ uri: item.img }} style={styles.itemProductImg} />
            </CardItem>
            <View style={styles.addWish}>
              <TouchableOpacity>
                <ANT name="heart" color={"#FF6347"} size={18} />
              </TouchableOpacity>
            </View>
            <CardItem style={{ paddingLeft: 5, paddingRight: 5, }}>
              <View style={{ width: '100%' }}>
                <Text numberOfLines={1} style={{ fontWeight: '600', }}>{item.item}</Text>
                <Text numberOfLines={1} style={styles.itemSubtitle}>{item.name}</Text>
                <View style={{ flexDirection: 'row', marginBottom: 5 }}>
                  <ANT name="star" color="#FFD700" size={15} style={{ paddingRight: 5 }} />
                  <Text note style={{ color: '#000' }}>{item.star}</Text>
                </View>
                <Text style={{ fontWeight: 'bold' }}>RM {item.price}</Text>
                <View style={{ alignItems: 'center', marginTop: 5 }}>
                  {
                    item.discount === '' ?
                      <View style={[styles.itemAddBtn, { borderColor: 'transperent', borderWidth: 0 }]}>
                        <Text note style={[styles.itemAddText, { color: 'transperent' }]}>{item.discount} </Text>
                      </View>
                      :
                      <View style={styles.itemAddBtn}>
                        <Text note style={styles.itemAddText}>{item.discount} OFF</Text>
                      </View>
                  }
                </View>
              </View>
            </CardItem>
          </Card>
        </TouchableOpacity>
      </View>
    )
  }


  render() {
    const navigateActions = this.props.navigation.navigate;
    console.log('CatagoryPage---', this.props)
    return (
      <View style={styles.container}>
        <ScrollView>
          {/************************************* TOP PRODUCT ****************************************/}
          <View style={{ backgroundColor: '#fff', paddingTop: 10 }}>
            <FlatList
              data={data}
              horizontal={true}
              renderItem={this._renderTop}
              keyExtractor={(item, index) => index.toString()}
            />
          </View>

          <View style={{ backgroundColor: '#fff', marginTop: 10 }}>
            {/************************************* PRODUCT 1 ****************************************/}
            <View style={styles.productTitle}>
              <View>
                <Text style={{ fontWeight: '600' }}>NEW ARRIVALS</Text>
                <Text note >100 Product</Text>
              </View>
              <TouchableOpacity onPress={() => navigateActions('product')}>
                <Text note style={{ fontWeight: '600', textAlign: 'right' }}>View All</Text>
              </TouchableOpacity>
            </View>
            <FlatList
              data={dataSeller}
              horizontal={true}
              renderItem={this._renderNew}
              keyExtractor={(item, index) => index.toString()}
            />

            {/************************************* PRODUCT 2 ****************************************/}
            <View style={[styles.productTitle, { marginTop: 20 }]}>
              <View>
                <Text style={{ fontWeight: '600' }}>HOT PICK</Text>
                <Text note >50 Product</Text>
              </View>
              <TouchableOpacity onPress={() => navigateActions('product')}>
                <Text note style={{ fontWeight: '600', textAlign: 'right' }}>View All</Text>
              </TouchableOpacity>
            </View>
            <FlatList
              data={dataSeller}
              horizontal={true}
              renderItem={this._renderNew}
              keyExtractor={(item, index) => index.toString()}
            />
          </View>

        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: { flex: 1 },
  itemWarp: { width: 160, padding: 5, paddingTop: 0, },
  itemProductImg: { height: 120, width: null, flex: 1 },
  itemText: { fontWeight: '600', width: '100%', textAlign: 'center', },
  itemTextWarp: { paddingLeft: 5, paddingRight: 5, },
  productTitle: {
    marginTop: 15, paddingBottom: 15, paddingLeft: 10, paddingRight: 10,
    flexDirection: 'row', justifyContent: 'space-between',
  },

  addWish: {
    position: 'absolute', top: '40%', right: 0, padding: 8, borderRadius: 100, zIndex: 1, elevation: 1, backgroundColor: 'transparent',
    // justifyContent : 'center', alignItems: 'center', flex: 1
  },
  itemWarp: { width: 200, padding: 5, paddingTop: 0 },
  itemSubtitle: { color: 'gray', marginBottom: 5, marginTop: 3 },
  itemAddWarp: { paddingLeft: 0, paddingRight: 0, paddingTop: 0, paddingBottom: 0 },
  itemAddBtn: { width: '80%', padding: 6, borderRadius: 30, borderColor: '#FF6347', borderWidth: .5 },
  itemAddText: { width: '100%', textAlign: 'center', color: '#FF6347', fontWeight: 'bold' },

});

export default CatagoryPage;
