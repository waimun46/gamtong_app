import React, { Component } from 'react';
import { View, StyleSheet, ScrollView, TouchableOpacity } from 'react-native';
import { Form, Item, Input, Label, Text, Textarea, Button } from 'native-base';
import { RFPercentage, RFValue } from "react-native-responsive-fontsize";
import ANT from 'react-native-vector-icons/AntDesign';
import ETY from 'react-native-vector-icons/Entypo';
import FEA from 'react-native-vector-icons/Feather';


class ContactPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  render() {
    const navigateActions = this.props.navigation.navigate;

    return (
      <View style={styles.container}>
        <ScrollView>
          <View style={{ backgroundColor: '#fff' }}>
            <View style={{ padding: 20 }}>
              <Text style={{ fontWeight: 'bold', fontSize: RFPercentage('3'), textAlign: 'center', }}>Get in Touch</Text>
            </View>
            <Form>
              <Item stackedLabel last>
                <Label>Your Name</Label>
                <Input />
              </Item>
              <Item stackedLabel last>
                <Label>Your Email</Label>
                <Input />
              </Item>
              <Item stackedLabel last>
                <Label>Your Phone</Label>
                <Input />
              </Item>
              <View style={{ width: '100%', paddingTop: 10 }}>
                <Textarea rowSpan={5} placeholder="Your Message" style={{ borderBottomWidth: .5, borderBottomColor: '#CCC' }} />
              </View>
            </Form>
            <View style={{ padding: 20 }}>
              <Button block dark style={{ height: 50 }}>
                <Text>CONTACT US</Text>
              </Button>
            </View>
          </View>

          <View style={{ padding: 20, marginTop: 20, paddingBottom: 10 }}>
            <View style={{ width: '100%' }}>
              <Text style={{ fontWeight: 'bold', textAlign: 'left' }}>GET SOCIAL</Text>
            </View>
            <View style={{ alignItems: 'flex-start' }}>
              <View style={{ marginTop: 10, width: '50%', alignItems: 'flex-start' }}>
                <View style={{ flexDirection: 'row', }}>
                  <TouchableOpacity>
                    <ANT name="facebook-square" size={25} style={{ textAlign: 'right', marginRight: 10,color:'#f48120' }} />
                  </TouchableOpacity>
                  <TouchableOpacity>
                    <ETY name="instagram" size={25} style={{ marginLeft: 10 , color:'#f48120'}} />
                  </TouchableOpacity>
                </View>
              </View>
            </View>
          </View>

          <View style={{ padding: 20 }}>
            <View style={{ width: '100%', marginBottom: 30 }}>
              <Text style={{ fontWeight: 'bold', textAlign: 'left' }}>PHONE</Text>
              <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: 10 }}>
                <FEA name="phone-call" size={20} style={{ textAlign: 'right', marginRight: 5,color:'#f48120' }} />
                <Text style={{ textAlign: 'left', }}>+60168104889</Text>
              </View>
            </View>
            <View style={{ width: '100%' }}>
              <Text style={{ fontWeight: 'bold', textAlign: 'left' }}>E-MAIL</Text>
              <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: 10, }}>
                <ANT name="mail" size={22} style={{ textAlign: 'right', marginRight: 5,color:'#f48120' }} />
                <Text style={{ textAlign: 'left', marginTop: -3}}>contact@gamtong.com</Text>
              </View>
            </View>
          </View>

          <View style={{ padding: 20 }}>
            <View style={{ width: '100%', marginBottom: 30 }}>
              <Text style={{ fontWeight: 'bold', textAlign: 'left' }}>ADDRESS</Text>
              <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: 10 }}>
                <ETY name="location-pin" size={20} style={{ textAlign: 'left', marginRight: 5, color:'#f48120' }} />
                <View style={{ paddingRight: 10 }}>
                  <Text style={{ textAlign: 'left', }}>Warisan Square, C-G-01, Block C, Ground Floor, Lot, A-G-18,
                 Jalan Tun Fuad Stephen, Taman Ria 1, 88000 Kota Kinabalu, Sabah</Text>
                </View>

              </View>
            </View>
          </View>

        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: { flex: 1, },
})

export default ContactPage;
