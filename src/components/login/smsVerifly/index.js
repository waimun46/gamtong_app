import React, { Component } from 'react';
import { View, ScrollView, StyleSheet, TextInput ,TouchableOpacity, Alert,  Modal, ActivityIndicator,} from 'react-native';
import { Text, Button } from 'native-base';
import { showMessage, hideMessage } from "react-native-flash-message";
import Countdown from 'react-countdown-now';
import AsyncStorage from '@react-native-community/async-storage';
import { FetchApi } from '../../../api';
import RNRestart from 'react-native-restart';




class SmsVerifly extends Component {
  constructor(props) {
    super(props);
    this.state = {
      code: '',
      mmidTokenForget: '',
      restartTime: Date.now() + 180000,
      // restartTime: Date.now() + 5000,
      isSubmit: false

    };
  }

  /****************************************** onChangeTextInput ********************************************/
  onChangeTextInput(text, field) {
    //console.log(text)
    if (field === 'code') { this.setState({ code: text }) }
  }

  /****************************************************** resendTacCode ********************************************************/
  resendTacCode() {
    let path = "member_login_generate_code";
    let parameters = { contact: this.props.navigation.state.params.phone };
    let ApiData = FetchApi(path, parameters);
    //console.log(ApiData, 'ApiData');

    this.setState({
      restartTime: Date.now() + 180000,
    })

    let that = this;

    fetch(ApiData).then((res) => res.json())
      .then(function (myJson) {
        //console.log(myJson, 'myJson-----------');
        if (myJson[0].status === 1) {
          Alert.alert('Tac Code already SMS your phone number', '',
            [
              {
                text: 'OK', onPress: () => that.setState({ isSubmit: false, })
              },
            ],
            { cancelable: false },
          );
        }
        else {
          Alert.alert(myJson[0].error, '',
            [
              {
                text: 'OK', onPress: () => that.setState({ isSubmit: false, })
              },
            ],
            { cancelable: false },
          );
        }
      })
  }

  /****************************************** renderer resend tac ********************************************/
  renderer = ({ minutes, seconds, completed }) => {
    if (completed) {
      return (
        <TouchableOpacity style={styles.resendBtn} onPress={() => this.resendTacCode()}>
          <Text note style={{ color: '#0095ff', }}>Resend TAC Code</Text>
        </TouchableOpacity>
      );
    }
    else {
      return (
        <View style={styles.timeCount}>
          <Text style={styles.secondText}>{minutes}:{seconds}</Text>
        </View>
      );
    }
  }

    /****************************************** onSubmit ********************************************/
    onSubmit() {
      const { code } = this.state;

      this.setState({
        isSubmit: true
      })
  
      if (code === '') {
        this.setState({
          error: showMessage({
            message: "Please Fill In Your Tac Code",
            description: "Error Messages",
            type: "danger",
            icon: 'warning',
          }),
          isSubmit: false
        })
      }
      else {
        const navigateActions = this.props.navigation.navigate;
        let path = "member_login_by_tacs";
        let parameters = {
          contact: this.props.navigation.state.params.phone,
          tac: this.state.code,
        };
        let ApiData = FetchApi(path, parameters);
  
  
        console.log('url', ApiData)
        console.log('parameters', parameters)
  
        fetch(ApiData).then((res) => res.json())
          .then((postData) => {
            if (postData[0].status === 1) {
              AsyncStorage.setItem('UUID_TOKEN', postData[0].UUID);
              this.setState({
                isSubmit: false
              })
              return RNRestart.Restart()
  
            } else {
              alert(postData[0].error)
              this.setState({
                isSubmit: false
              })
            }
          })
  
      }
    }


  render() {
    const { code, restartTime, isSubmit } = this.state;
console.log(this.props.navigation.state.params.uuidToken, 'token')
    return (
      <ScrollView>

        {/*************************************** submit loading ****************************************/}
        {
            isSubmit ?
              <Modal
                transparent={true}
                animationType={'none'}
                visible={isSubmit}
                onRequestClose={() => { console.log('close modal') }}>
                <View style={styles.modalBackground}>
                  <View style={styles.activityIndicatorWrapper}>
                    <ActivityIndicator size="large" animating={isSubmit} />
                  </View>
                </View>
              </Modal>
              : null
          }

        <View style={{ flex: 1, alignItems: 'center', backgroundColor:' #fff' }}>
          {/************************** code input ****************************/}
          <View style={{ width: '80%', marginTop: 30 }}>
            <TextInput
              value={code}
              placeholder="******"
              placeholderTextColor="#8c8c8c"
              style={styles.inputsty}
              onChangeText={(text) => this.onChangeTextInput(text, 'code')}
              autoCorrect={false}
              autoCapitalize="none"
              keyboardType={'numeric'}
            />

            <Text note>
              We have sent you an SMS with a code to the number above,
              To complete your phone number verification, please enter the 6-digit activation code.
          </Text>

            {/************************** Countdown ****************************/}
            <View style={{ marginTop: 10 }}>
              <Countdown
                date={restartTime}
                renderer={this.renderer}
                key={restartTime}
              />
            </View>

            {/************************** button submit ****************************/}
            <Button full dark style={styles.btnwarp} onPress={() => this.onSubmit()}>
              <Text style={styles.btntext}>VERIFY</Text>
            </Button>


          </View>

        </View>

      </ScrollView>
    );
  }
}


const styles = StyleSheet.create({
  inputsty: {
    height: 50, borderColor: '#ccc', borderWidth: .5, padding: 10, borderRadius: 5, color: '#000',
    marginBottom: 20, textAlign: 'center', letterSpacing: 10
  },
  btnwarp: { marginTop: 30, borderRadius: 5, height: 50, },
  btntext: { color: '#fff', },
  resendBtn: { height: 30, justifyContent: 'center', alignItems: 'flex-end' },
  timeCount: { height: 30, justifyContent: 'flex-end', alignItems: 'flex-end' },
  secondText: { color: 'red', fontSize: 18, textAlign: 'right' },
  modalBackground: { flex: 1, alignItems: 'center', flexDirection: 'column', justifyContent: 'space-around', backgroundColor: '#00000040' },
  activityIndicatorWrapper: {
    backgroundColor: '#FFFFFF', height: 100, width: 100, borderRadius: 10, display: 'flex',
    alignItems: 'center', justifyContent: 'space-around'
  },

});

export default SmsVerifly;
