import React, { Component } from 'react';
import { View, StyleSheet, ScrollView, TouchableOpacity, Image, Modal, ActivityIndicator, Alert } from 'react-native';
import { Form, Item, Input, Label, Button, Text } from 'native-base';
import MATI from 'react-native-vector-icons/MaterialCommunityIcons';
import FA from 'react-native-vector-icons/FontAwesome';
import logo from '../../assest/logo/logo1.png';
import { FetchApi } from '../../api';
import { showMessage, hideMessage } from "react-native-flash-message";
import AsyncStorage from '@react-native-community/async-storage';
import RNRestart from 'react-native-restart';



class Loginpage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      phone: '',
      password: '',
      isSubmit: false
    };
  }

  /****************************************** onChangeTextInput ********************************************/
  onChangeTextInput(text, field) {
    //console.log(text)
    if (field === 'phone') { this.setState({ phone: text }) }
    if (field === 'password') { this.setState({ password: text }) }
  }

  /****************************************** onSubmit ********************************************/
  onSubmit() {
    const { phone, password } = this.state;

    this.setState({
      isSubmit: true
    })

    if (phone === '') {
      this.setState({
        error: showMessage({
          message: "Please Fill In Your Phone Number",
          description: "Error Messages",
          type: "danger",
          icon: 'warning',
        }),
        isSubmit: false
      })
    }
    else if (password == '') {
      this.setState({
        error: showMessage({
          message: "Please Fill In Your Password",
          description: "Error Messages",
          type: "danger",
          icon: 'warning',
        }),
        isSubmit: false
      })
    }
    else {
      const navigateActions = this.props.navigation.navigate;
      let path = "member_login";
      let parameters = {
        contact: phone,
        password: password,
      };
      let ApiData = FetchApi(path, parameters);


      console.log('url', ApiData)
      console.log('parameters', parameters)

      fetch(ApiData).then((res) => res.json())
        .then((postData) => {
          if (postData[0].status === 1) {
            AsyncStorage.setItem('UUID_TOKEN', postData[0].UUID);
            this.setState({
              isSubmit: false,
              phone: '',
              password: ''
            })
            return RNRestart.Restart()

          } else {
            Alert.alert(postData[0].error, '',
              [
                {
                  text: 'OK', onPress: () => that.setState({
                    isSubmit: false,
                    phone: '',
                    password: ''
                  })
                },
              ],
              { cancelable: false },
            );
            this.setState({
              isSubmit: false
            })
          }
        })

    }
  }


  render() {
    const { phone, password, isSubmit } = this.state;
    const navigateActions = this.props.navigation.navigate;

    console.log('phone', phone)
    console.log('password', password)

    return (
      <View style={styles.container}>
        <ScrollView>
          {/*************************************** submit loading ****************************************/}
          {
            isSubmit ?
              <Modal
                transparent={true}
                animationType={'none'}
                visible={isSubmit}
                onRequestClose={() => { console.log('close modal') }}>
                <View style={styles.modalBackground}>
                  <View style={styles.activityIndicatorWrapper}>
                    <ActivityIndicator size="large" animating={isSubmit} />
                  </View>
                </View>
              </Modal>
              : null
          }

          <View style={{ backgroundColor: '#f48120', height: 150, justifyContent: 'center', alignItems: 'center', paddingBottom: 30 }}>
            <Image source={logo} style={{ width: '70%', height: '100%', resizeMode: 'contain' }} />
          </View>
          <View style={{ padding: 20, paddingLeft: 10 }}>
            <Form style={{ paddingRight: 0 }} >
              <Item stackedLabel >
                <Label >Phone Number</Label>
                <Input
                  value={phone}
                  placeholder="EXP: 0123456789"
                  placeholderTextColor="#ccc"
                  onChangeText={(text) => this.onChangeTextInput(text, 'phone')}
                  autoCorrect={false}
                  autoCapitalize="none"
                  //secureTextEntry={true}
                  keyboardType={'numeric'}
                  style={{ fontSize: 15 }}
                />
              </Item>
              <Item stackedLabel >
                <Label>Password</Label>
                <Input
                  value={password}
                  placeholder="Password"
                  placeholderTextColor="#ccc"
                  onChangeText={(text) => this.onChangeTextInput(text, 'password')}
                  autoCorrect={false}
                  autoCapitalize="none"
                  secureTextEntry={true}
                  style={{ fontSize: 15 }}
                />
              </Item>
            </Form>
          </View>

          <View style={{ padding: 20 }}>
            <Button onPress={() => this.onSubmit()} block style={{ backgroundColor: '#f48120', height: 50 }}>
              <Text>MEMBER LOGIN</Text>
            </Button>
            <Button full dark style={styles.btnwarpSms} onPress={() => navigateActions('sms')}>
              <FA name='envelope' style={{ color: '#fff', paddingRight: 20 }} size={25} />
              <Text style={styles.btntext}>Login With SMS</Text>
            </Button>
          </View>

          <View style={{ paddingRight: 20, paddingBottom: 10, paddingTop: 10 }}>
            <TouchableOpacity onPress={() => navigateActions('forgotPass')}>
              <Text note style={{ textAlign: 'right', color: '#00BFFF' }}>Forget Password ?</Text>
            </TouchableOpacity>
          </View>

          <View style={{ alignItems: 'center', marginTop: 20, }}>
            <View style={{ flexDirection: 'row', }}>
              <Text note >Don't have a account ? </Text>
              <TouchableOpacity onPress={() => navigateActions('member')}>
                <Text note style={{ color: '#f48120', fontWeight: 'bold' }}>Join Member</Text>
              </TouchableOpacity>
            </View>

          </View>

        </ScrollView>
      </View>

    );
  }
}

const styles = StyleSheet.create({
  container: { backgroundColor: '#fff', flex: 1 },
  btnwarpSms: { height: 50, marginTop: 20, borderRadius: 5 },
  btntext: { color: '#fff', },
  modalBackground: { flex: 1, alignItems: 'center', flexDirection: 'column', justifyContent: 'space-around', backgroundColor: '#00000040' },
  activityIndicatorWrapper: {
    backgroundColor: '#FFFFFF', height: 100, width: 100, borderRadius: 10, display: 'flex',
    alignItems: 'center', justifyContent: 'space-around'
  },

})


export default Loginpage;
