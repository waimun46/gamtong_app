import React, { Component } from 'react';
import { View, StyleSheet, FlatList, TouchableOpacity } from 'react-native';
import { Button, Text, List, ListItem, Item, Input } from 'native-base';


const data = [
  {
    type: 'A',
    brands: [{ brand: 'Apple01' }, { brand: 'Apple02' }, { brand: 'Apple03' },]
  },
  {
    type: 'B',
    brands: [{ brand: 'Banana01' }, { brand: 'Banana02' }, { brand: 'Banana03' }, { brand: 'Banana04' },]
  },
  {
    type: 'C',
    brands: [{ brand: 'Cat01' }, { brand: 'Cat02' }, { brand: 'Cat03' }, { brand: 'Cat04' }, { brand: 'Cat05' },]
  },

]


class BrandsPage extends Component {
  constructor(props) {
    super(props);
    this.state = {

    };
  }

  /**************************************** renderSlider ************************************/
  _renderItem = ({ item, index }) => {
    const navigateActions = this.props.navigation.navigate;

    return (
      <List key={index}>
        <ListItem itemDivider >
          <Text>{item.type}</Text>
        </ListItem>
        {
          item.brands.map((list, i) => {
            return (

              <ListItem key={i}>
                <TouchableOpacity onPress={() => navigateActions('product')} style={{
                  alignItems: 'flex-start', flex: 1, justiflyContent: 'flex-start'
                }}>
                  <Text style={{textAlign: 'left',   width: '100%', }}>{list.brand}</Text>
                </TouchableOpacity>
              </ListItem>

            )
          })
        }
      </List>
    );
  }

  render() {
    console.log('data', data)
    return (
      <View style={styles.container}>
        
          <FlatList
            data={data}
            renderItem={this._renderItem}
            keyExtractor={(item, index) => index.toString()}
          />
      
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: { backgroundColor: '#fff', flex: 1 },


});

export default BrandsPage;
