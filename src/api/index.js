export function FetchApi(path, parameters) {
  let query = "";
  let security = "sasaapp20181019"
  let url = "https://fishmarketrestaurantkk.com/gt/api/";
  for (let key in parameters) {
    let value = parameters[key];
    query += encodeURIComponent(key) + "=" + encodeURIComponent(value) + "&";
  }
  if (query.length > 0) {
    query = query.substring(0, query.length - 1);
    url = url + path + ".php" + "?" + "secode" + "=" + security + "&" + query;
  }
  return url;
}


export function FetchApiNoParam(path) {
  let security = "sasaapp20181019"
  let url = "https://fishmarketrestaurantkk.com/gt/api/";
  url = url + path + ".php" + "?" + "secode" + "=" + security

  return url;
}


